package com.jammarandhattab.Utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

import java.io.ByteArrayOutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class MyUtils {

    public static String getDateTime(String Format)
    {
        SimpleDateFormat sdf=new SimpleDateFormat(Format);
        String date=sdf.format(Calendar.getInstance().getTime());
        return date;
    }

    public static Bitmap DecodeImagefromBase64(String encodedImage) {
        if (encodedImage!=null) {

            byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            return decodedByte;
        }
        return null;


    }

    public static String EncodeImageinBase64(Bitmap myBitmap) {
        if (myBitmap!=null) {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 55, bytes);
            //return (bytes.toByteArray());
            return Base64.encodeToString(bytes.toByteArray(), Base64.DEFAULT);
        }
        return null;
    }

    public static String DecimalFormatedString(String value) {
        DecimalFormat formatter = new DecimalFormat("#,###,###");
        String FormatedString = formatter.format(Integer.parseInt(value));
        return FormatedString;
    }
}
