package com.jammarandhattab.Views.Activities;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jammarandhattab.Helper.MySingelton;
import com.jammarandhattab.Views.Adapters.CartRecyclerAdapter;
import com.jammarandhattab.Views.Adapters.ImageSliderAdapter;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.Utils.Common;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class ItemPreview extends AppCompatActivity {

    ViewPager viewPager;
    int currentPage = 0;
    int NUM_PAGES = 0;
    CirclePageIndicator indicator;
    TextView tvTitle,tvPrice,tvQty;
    Button btnAddtoCart,btnPlus,btnMinus;
    LinearLayout llPlusMinus;
    int counter;
    LinearLayout bottomSheetLayout;
    ArrayList<Integer> imageList=new ArrayList<>();
    ImageView btnExpand;
RecyclerView rvbottomsheet;
    RelativeLayout rlItemOReview;
    CoordinatorLayout coordinatorLayout;

    BottomSheetBehavior bottomSheetBehavior;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_preview);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initComponents();
        initImageSlider();
        ClickEvents();




    }

    private void initComponents() {
        viewPager=findViewById(R.id.ItemPreview_viewPager);
        indicator=findViewById(R.id.ItemPreview_indicator);
        tvTitle=findViewById(R.id.ItemPreview_tvTitle);
        tvPrice=findViewById(R.id.ItemPreview_tvPrice);
        tvQty=findViewById(R.id.ItemPreview_tvQty);
        btnAddtoCart=findViewById(R.id.ItemPreview_btnAddtoCart);
        btnPlus=findViewById(R.id.ItemPreview_btnPlus);
        btnMinus=findViewById(R.id.ItemPreview_btnMinus);
        llPlusMinus=findViewById(R.id.ItemPreview_llPlusMinus);
        rlItemOReview=findViewById(R.id.RL_itemPreview);
        bottomSheetLayout=findViewById(R.id.subMenu_bottomSheet);
        bottomSheetBehavior=BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetLayout.setBackgroundColor(getResources().getColor(R.color.white));
        coordinatorLayout=findViewById(R.id.itemPreview_coordinator);

        btnExpand=findViewById(R.id.bottomsheet_imgexpand);
    }

    private void ClickEvents(){
        btnAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // bottomSheetLayout.expand();
                counter++;
                btnAddtoCart.setVisibility(View.GONE);
                llPlusMinus.setVisibility(View.VISIBLE);
                tvQty.setText(String.valueOf(counter));


            }
        });

        btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                counter++;
                tvQty.setText(String.valueOf(counter));
            }
        });

        btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   bottomSheetLayout.collapse();
                counter--;
                if (counter>0)
                {
                    tvQty.setText(String.valueOf(counter));
                }
                else
                {
                    llPlusMinus.setVisibility(View.GONE);
                    btnAddtoCart.setVisibility(View.VISIBLE);

                }

            }
        });


        btnExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//               if (bottomSheetLayout.isExpended())
//               {
//                   bottomSheetLayout.collapse();
//                   rlItemOReview.setVisibility(View.VISIBLE);
//                   btnExpand.setImageResource(R.drawable.ic_action_expand);
//
//               }
//               else
//               {
//                   bottomSheetLayout.expand();
//                   rlItemOReview.setVisibility(View.GONE);
//                   btnExpand.setImageResource(R.drawable.ic_action_collapse);
//
//               }
                if (bottomSheetBehavior.getState()!=BottomSheetBehavior.STATE_EXPANDED)
                {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                }else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                }

            }
        });



        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
//                        btnExpand.setImageResource(R.drawable.ic_action_collapse);
                        btnExpand.animate().rotationBy(180).setDuration(300).setInterpolator(new LinearInterpolator()).start();
//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
                        showCartRecycler();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        btnExpand.setImageResource(R.drawable.ic_action_expand);
                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
                        btnExpand.animate().rotationBy(180).setDuration(300).setInterpolator(new LinearInterpolator()).start();
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

    }

    private void initImageSlider(){
        if (imageList.size()>0)
            imageList.clear();

//        imageList.add("http://www.aweermart.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/g/f/gf.jpg");
//        imageList.add("https://square-production.s3.amazonaws.com/files/d57e0059af354957ee8e8449809afde9/original.jpeg");
//        imageList.add("https://www.koshersnowconesyrup.com/wp-content/uploads/2014/06/sour-green-apple.jpg");
//        imageList.add("http://cjsmarket.com/wp-content/uploads/2016/09/Granny-Smith.png");
//        imageList.add("https://cdn.pixabay.com/photo/2018/01/29/22/56/apple-3117507_960_720.jpg");
//        imageList.add("https://5.imimg.com/data5/YY/EN/MY-8155364/fresh-apple-500x500.jpg");

        //  Common.SliderImages.add("https://stmed.net/sites/default/files/pizza-wallpapers-28292-7827455.jpg");
//        Common.sliderImages.add("https://rialto-lieferservice.de/wp-content/uploads/2017/07/Special-Pizza-HD-Wallpaper-00674.jpg");
//        Common.sliderImages.add("http://www.theuglyduckout.com.au/wp-content/uploads/2016/09/Chicken-Burger-sized-1.jpg");
//        Common.sliderImages.add("https://images5.alphacoders.com/433/433534.jpg");
//        Common.sliderImages.add("https://hdwallsource.com/img/2014/7/pizza-wallpaper-20437-20948-hd-wallpapers.jpg");
//        Common.sliderImages.add("https://matram.hr/upload/publish/60/salate-i-dressing-recepti-2_58dcdb0925282.jpg");
        //Set the pager with an adapter

        imageList.add(R.drawable.slider_a);
        imageList.add(R.drawable.slider_a);

        viewPager.setAdapter(new ImageSliderAdapter(this, MySingelton.getInstance().getSliderPhotos()));



        indicator.setViewPager(viewPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES =Common.sliderImages.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);


        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    public void showCartRecycler() {

        rvbottomsheet = findViewById(R.id.bottomSheet_rv);
        List<Cart> Cartlist = MainActivity.myAppDatabase.dao().getCart();
        if (Cartlist.size() > 0) {
            rvbottomsheet.setHasFixedSize(true);
            rvbottomsheet.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvbottomsheet.setAdapter(new CartRecyclerAdapter(this, Cartlist));
        } else {
            Toast.makeText(this, "No Item in Cart", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
