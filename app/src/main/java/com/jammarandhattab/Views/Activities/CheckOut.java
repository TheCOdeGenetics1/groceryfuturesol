package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.jammarandhattab.Helper.Constants;
import com.jammarandhattab.Models.AddItemsResponse;
import com.jammarandhattab.Models.CheckoutResponse;
import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Presenters.CheckoutPresenter;
import com.jammarandhattab.Presenters.LoginPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.Helper.UserSession;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.interfaces.ICheckOut;
import com.jammarandhattab.interfaces.ILogin;

import java.util.List;

import retrofit2.Response;

public class CheckOut extends AppCompatActivity implements ICheckOut.iMain, ILogin.iMain {
    EditText etMobile,etName,etEmail,etAddress, etLandmark,etAdditionalInfo,etPaymentType;
    Button btnCheckout;
    CheckoutPresenter checkoutPresenter;
    LoginPresenter loginPresenter;
    String contactID;
    String totalPrice;
    ProgressBar pb;
    boolean isCartFinished=false;

    String customerID;

    private String TAG="Checkout Activity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        initViews();
        contactID=UserSession.getInstance(this).getCustomerID();
        totalPrice=getIntent().getStringExtra("TotalPrice");



        if (!UserSession.getInstance(this).getMobile().isEmpty()) {
            etMobile.setText(UserSession.getInstance(this).getMobile());
        }

        if (!UserSession.getInstance(this).getEmail().isEmpty()){
            etEmail.setText(UserSession.getInstance(this).getEmail());
        }

        if (!UserSession.getInstance(this).getName().isEmpty()){
            etName.setText(UserSession.getInstance(this).getName());
        }

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFileds()){
                    customerID=UserSession.getInstance(CheckOut.this).getCustomerID();
                    if (customerID.isEmpty()){
                        loginPresenter.doRegistration(etMobile.getText().toString(),"123456",
                                etName.getText().toString(),Constants.websiteId);
                    }else {
                        checkoutPresenter.checkOut(Constants.websiteId,customerID,"",
                                etAddress.getText().toString(),
                                etLandmark.getText().toString(),
                                etName.getText().toString(),
                                "Delivery",
                                "Cash on delivery",
                                "Pending",
                                totalPrice,
                                etMobile.getText().toString());
                    }


                }
            }
        });
    }

    private void initViews() {
        etMobile=findViewById(R.id.checkout_etMobile);
        etName=findViewById(R.id.checkout_etName);
        etEmail=findViewById(R.id.checkout_etEmail);
        etAddress=findViewById(R.id.checkout_etAddress);
        etLandmark =findViewById(R.id.checkout_etLandmark);
        etAdditionalInfo=findViewById(R.id.checkout_etAdditionalInfo);
        etPaymentType=findViewById(R.id.checkout_etDileveryMethod);
        btnCheckout=findViewById(R.id.checkout_btnCheckout);
        pb=findViewById(R.id.pb);
        checkoutPresenter=new CheckoutPresenter(this);
        loginPresenter=new LoginPresenter(this);
    }

    private boolean validateFileds(){
        if (etName.getText().toString().isEmpty()){
            etName.setError("Name should'nt be empty");
            return false;
        }else if(etMobile.getText().toString().isEmpty()){
            etMobile.setError("Mobile No. should'nt be empty");
            return false;

        }
//        else if (etEmail.getText().toString().isEmpty()){
//            etEmail.setError("Email should'nt be empty");
//            return false;
//        }
        else if (etAddress.getText().toString().isEmpty()){
            etAddress.setError("Address should'nt be empty");
            return false;
        }
        else if (etLandmark.getText().toString().isEmpty()){
            etLandmark.setError("Landmark should'nt be empty");
            return false;
        }

        return true;
    }


    @Override
    public void showProgress() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(Throwable t) {
        Snackbar.make(btnCheckout,"Something went wrong",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               loginPresenter.doRegistration(etMobile.getText().toString(),"123456",
                       etName.getText().toString(),Constants.websiteId);
            }
        }).show();
        Log.e(TAG, "showError: ",t.getCause() );
    }

    @Override
    public void setLoginResponse(Response<LoginResponse> response) {
    if (response.body()!=null){
        checkoutPresenter.checkOut(Constants.websiteId,String.valueOf(response.body().getCustomerID()),"",
                etAddress.getText().toString(),
                etLandmark.getText().toString(),
                etName.getText().toString(),
                "Delivery",
                "Cash on delivery",
                "Pending",
                totalPrice,
                etMobile.getText().toString());

        UserSession.getInstance(CheckOut.this).setCustomerID(String.valueOf(response.body().getCustomerID()));
        UserSession.getInstance(CheckOut.this).setName(etName.getText().toString());
        UserSession.getInstance(CheckOut.this).setMobile(etMobile.getText().toString());
        UserSession.getInstance(CheckOut.this).setEmail(etEmail.getText().toString());

    }else {
        Snackbar.make(btnCheckout,"Something went wrong",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginPresenter.doRegistration(etMobile.getText().toString(),"123456",
                        etName.getText().toString(),Constants.websiteId);
            }
        }).show();
    }

    }

    @Override
    public void setCheckoutResponse(Response<CheckoutResponse> response) {
        if (response.body()!=null){
            addItemsToCheckOut(response.body().getOrderID());
        }
    }

    private void addItemsToCheckOut(Integer orderID) {
        List<Cart> cart=MainActivity.myAppDatabase.dao().getCart();
        for (int i=0;i<cart.size();i++){
            checkoutPresenter.addItemtoCheckout(Constants.websiteId, orderID,cart.get(i).getP_Id(),
                    cart.get(i).getPrice(),cart.get(i).getQuantity());
            if (i==cart.size()-1){
                isCartFinished=true;
            }
        }

        if (isCartFinished){
            Intent intent=new Intent(this,Thankyou.class);
            intent.putExtra("Name",etName.getText().toString());
            intent.putExtra("OrderID",String.valueOf(orderID));
            startActivity(intent);
            finish();
        }

    }

    @Override
    public void addItemsResponse(Response<AddItemsResponse> response) {

    }

    @Override
    public void hideProgress() {
        pb.setVisibility(View. GONE);
    }


}
