package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jammarandhattab.Models.dbproduct;
import com.jammarandhattab.Views.Adapters.CartRecyclerAdapter;
import com.jammarandhattab.Views.Adapters.ProductRecyclerAdapter;
import com.jammarandhattab.Views.Adapters.SubmenuRecyclerAdapter;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.Room.SavedList;
import com.jammarandhattab.Utils.Common;
import com.jammarandhattab.Utils.MyUtils;
import com.yarolegovich.lovelydialog.LovelyTextInputDialog;

import java.util.ArrayList;
import java.util.List;

public class SubMenu extends AppCompatActivity implements ProductRecyclerAdapter.onQuantitychange {
    RecyclerView rv;
    LinearLayout bottomSheetLayout;
    BottomSheetBehavior bottomSheetBehavior;
    ImageView btnExpand;
    RecyclerView rvbottomsheet;
    RelativeLayout rlTop;
    CoordinatorLayout coordinatorLayout;

    TextView tvTotalItemTop,tvTotalAmountTop,tvSubTotal,tvDiscount,tvDeliveryCharges,tvGrandTotal;
    Button btnSaveList,btnCheckout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initComponents();
        clickEvents();
        setUpRecyclerView();
        showCartRecycler();

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

    }



    public void initComponents() {
        rv=findViewById(R.id.submenu_rv);
        btnExpand=findViewById(R.id.bottomsheet_imgexpand);
        bottomSheetLayout=findViewById(R.id.subMenu_bottomSheet);
        coordinatorLayout=findViewById(R.id.coordinator);
        rlTop=findViewById(R.id.bottomSheetRLtop);
        bottomSheetBehavior=BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetLayout.setBackgroundColor(getResources().getColor(R.color.white));
        rvbottomsheet=findViewById(R.id.bottomSheet_rv);
        tvTotalItemTop=findViewById(R.id.bottomsheet_tvTotalItemTop);
        tvTotalAmountTop=findViewById(R.id.bbottomsheet_tvTotalAmountTop);
        tvSubTotal=findViewById(R.id.bottomsheet_tvsubtotal);
        tvDiscount=findViewById(R.id.bottomsheet_tvDiscount);
        tvDeliveryCharges=findViewById(R.id.bottomsheet_tvDeliveryCharges);
        tvGrandTotal=findViewById(R.id.bottomsheet_tvGrandTotal);
        btnSaveList=findViewById(R.id.bottomsheet_btnSaveList);
        btnCheckout=findViewById(R.id.bottomsheet_btnCheckout);

    }

    private void clickEvents() {
        btnExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bottomSheetBehavior.getState()!=BottomSheetBehavior.STATE_EXPANDED)
                {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                }else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                    coordinatorLayout.setVisibility(View.INVISIBLE);
                    coordinatorLayout.setVisibility(View.VISIBLE);

                }
            }



        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        btnExpand.setImageResource(R.drawable.ic_action_collapse);
                        // btnExpand.setRotation(180f);

                        // btnExpand.animate().rotationBy(180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
                        showCartRecycler();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        btnExpand.setImageResource(R.drawable.ic_action_expand);

                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
//                        btnExpand.animate().rotationBy(-180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
                        //  btnExpand.setRotation(360f);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {


            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SubMenu.this,SelectAdress.class));
            }
        });



    }

    public  void showCartRecycler() {
        double totalAmount = 0,totalItems = 0;
        CartRecyclerAdapter adapter;

        List<Cart> Cartlist=MainActivity.myAppDatabase.dao().getCart();
        if (Cartlist.size()>0) {
            rvbottomsheet.setHasFixedSize(true);
            rvbottomsheet.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvbottomsheet.setAdapter(new CartRecyclerAdapter(this, Cartlist));
        }else {
            rvbottomsheet.setHasFixedSize(true);
            rvbottomsheet.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvbottomsheet.setAdapter(new CartRecyclerAdapter(this, Cartlist));
        }

        for (int i=0;i<Cartlist.size();i++)
        {
            totalItems=totalItems+Double.parseDouble(Cartlist.get(i).getQuantity());
            totalAmount=totalAmount+(Double.parseDouble(Cartlist.get(i).getQuantity())*Double.parseDouble(Cartlist.get(i).getPrice()));
        }

        tvTotalItemTop.setText(String.valueOf(totalItems));
        tvSubTotal.setText("PKR "+String.valueOf(totalAmount));
        tvTotalAmountTop.setText("PKR "+String.valueOf(totalAmount));



    }

    private void setUpRecyclerView() {
        if (Common.productList.size()!=0)
            Common.productList.clear();
        Common.productList.add(new dbproduct("Grocery","Apple 1kg","1 کلوگرام سیب","100","https://media.istockphoto.com/photos/green-apple-picture-id584226186?k=6&m=584226186&s=612x612&w=0&h=7so5JvOXbcJunrq5J9FjcL-e1zDDhSR7LNskmdkMEUs=",""));
        Common.productList.add(new dbproduct("Frozen Food","Meat 1kg","1 کلو گوشت","1000","https://media.istockphoto.com/photos/frozen-raw-pork-neck-chops-meat-steak-picture-id873818672?k=6&m=873818672&s=612x612&w=0&h=TsqQa2TOEvZlvlm5wN-PiNEBQB_gndfR0L8GBPuI9QQ=",""));
        Common.productList.add(new dbproduct("Vegetables","LadyFinger","500 گرام لیڈی فنگر","123","https://qph.fs.quoracdn.net/main-qimg-cd0e71ca0eb5f85dc054e167c8486266-c",""));
        Common.productList.add(new dbproduct("Bakery","Biscuit","1 پیک بسکٹ","1232","https://d1doqjmisr497k.cloudfront.net/-/media/schwartz/recipes/2000x1125/ginger_crunch_biscuits_2000.ashx?vd=20180522T020241Z&ir=1&width=885&height=498&crop=auto&quality=75&speed=0&hash=AAB02576666F664E19A7952F4B63088E292F8ABF",""));
        Common.productList.add(new dbproduct("Cooking","Cooking Oil","1 پیک کھانا پکانا تیل","1245","https://sc02.alicdn.com/kf/HTB1.6YlLpXXXXcOXVXXq6xXFXXXV/Tuong-An-Cooking-Oil-2L.jpg",""));


        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rv.setAdapter(new SubmenuRecyclerAdapter(this,this));

    }

    public void SaveListONClick(View view)
    {

        new LovelyTextInputDialog(this)
                .setTitle("Enter List Name")
                .setIcon(R.drawable.ic_action_favourite)

                .setConfirmButton("Save", new LovelyTextInputDialog.OnTextInputConfirmListener() {
                    @Override
                    public void onTextInputConfirmed(String text) {
                        if (text != null) {

                            String date = MyUtils.getDateTime("MMM dd,yyyy");
                            List<Cart> cart = new ArrayList<>();
                            cart = MainActivity.myAppDatabase.dao().getCart();
                            for (int i = 0; i < cart.size(); i++) {
                                SavedList list = new SavedList();
                                list.setCategory(cart.get(i).getCategory());
                                list.setItemName(cart.get(i).getTitle());
                                list.setUrduName(cart.get(i).getOtherTitle());
                                list.setQty(Integer.parseInt(cart.get(i).getQuantity()));
                                list.setPrice(Integer.parseInt(cart.get(i).getPrice()));
                                list.setImage(cart.get(i).getImageUrl());
                                list.setDate(date);
                                list.setListTitle(text);
                                MainActivity.myAppDatabase.dao().addintoSavedList(list);
                            }
                            Toast.makeText(SubMenu.this, "List Saved", Toast.LENGTH_SHORT).show();
                            MainActivity.myAppDatabase.dao().deleteCart();
                            finish();
                        }else {
                            Toast.makeText(SubMenu.this, "Name Should'nt be empty", Toast.LENGTH_SHORT).show();
                        }
                    }

                }).setNegativeButton("Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        })
                .show();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onChangedQuantity() {
        //Toast.makeText(this, "show cart Recycler", Toast.LENGTH_SHORT).show();
        showCartRecycler();
    }
}
