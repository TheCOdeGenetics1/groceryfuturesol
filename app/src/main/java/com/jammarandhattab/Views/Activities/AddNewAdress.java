package com.jammarandhattab.Views.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jammarandhattab.interfaces.AddAdress;
import com.jammarandhattab.R;

public class AddNewAdress extends AppCompatActivity  {
    AddAdress addAdress;
    Button btnSave;
    TextView tvTitle,tvHouseNo,tvStreetNo,tvArea,tvCity;

    public void setListener(AddAdress listener)
    {
        this.addAdress=listener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_adress);
        initComponents();



        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              btnSaveOnCLick();
            }
        });



    }

    private void initComponents() {
        tvTitle=findViewById(R.id.addNewAdress_tvTitle);
        tvHouseNo=findViewById(R.id.addNewAdress_tvHouseNo);
        tvStreetNo=findViewById(R.id.addNewAdress_tvStreetNo);
        tvArea=findViewById(R.id.addNewAdress_tvArea);
        tvCity=findViewById(R.id.addNewAdress_tvCity);
        btnSave=findViewById(R.id.addNewAdress_btnSave);
    }

    void btnSaveOnCLick(){
        String title = null,Adress = null;
        title=tvTitle.getText().toString();
        Adress=tvHouseNo.getText().toString()+" "+tvStreetNo.getText().toString()+" "+tvArea.getText().toString()+","+tvCity.getText().toString();



//        addAdress.onAdressEntered(title,Adress);
       // finish();
    }


}
