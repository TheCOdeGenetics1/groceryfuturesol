package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Presenters.LoginPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.interfaces.ILogin;

import retrofit2.Response;

public class Registration extends AppCompatActivity implements View.OnClickListener,ILogin.iMain {
    private  final String TAG = "Registartion Activity";
    EditText etName, etMobile,etPassword;
    View viewSignIn;
    Button btnSignup;
    ProgressBar pb;
    LoginPresenter loginPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initViews();
    }

    private void initViews() {
        etName=findViewById(R.id.reg_etname);
        etMobile=findViewById(R.id.reg_etMobile);
        etPassword=findViewById(R.id.reg_etPassword);
        viewSignIn=findViewById(R.id.reg_viewSignin);
        btnSignup=findViewById(R.id.reg_btnSignup);
        pb=findViewById(R.id.pb);
        loginPresenter=new LoginPresenter(this);
        viewSignIn.setOnClickListener(this);
        btnSignup.setOnClickListener(this);
    }

    private boolean validateFields(){
        if (etName.getText().toString().isEmpty()){
            etName.setError("Name should'nt be empty");
            return false;
        }else if(etMobile.getText().toString().isEmpty()){
            etMobile.setError("Mobile No. should'nt be empty");
            return false;

        }
        else if (etPassword.getText().toString().isEmpty()){
            etPassword.setError("Password should'nt be empty");
            return false;
        }
        else if (etPassword.getText().toString().length()<6){
            etPassword.setError("Password is too short");
            return false;
        }
        return true;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.reg_viewSignin:
                startActivity(new Intent(this,LoginActivity.class));
                break;
            case R.id.reg_btnSignup:
                if (validateFields())
                loginPresenter.doRegistration(
                        etMobile.getText().toString(),
                        etPassword.getText().toString(),
                        etName.getText().toString(),1);
                break;

        }

    }

    @Override
    public void showProgress() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pb.setVisibility(View.GONE);
    }

    @Override
    public void setLoginResponse(Response<LoginResponse> response) {
        if (response.body()!=null){
            Toast.makeText(this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this,LoginActivity.class));
            finish();
        }else {
            Toast.makeText(this, "Error in Registration", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void showError(Throwable t) {
        Log.e(TAG, "showError: ",t.getCause() );
    }
}
