package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jammarandhattab.interfaces.AddAdress;
import com.jammarandhattab.Views.Adapters.SelectAdressRecyler;
import com.jammarandhattab.Views.Fragments.dbAdress;
import com.jammarandhattab.R;

import java.util.ArrayList;
import java.util.List;

public class SelectAdress extends AppCompatActivity implements AddAdress {
    RecyclerView rvAdress;
    List<dbAdress> adressList=new ArrayList<>();
    Button btnContinue;
    AddNewAdress addNewAdress;
    TextView tvNewAdress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_adress);
        rvAdress=findViewById(R.id.selectAdress_rv);
        btnContinue =findViewById(R.id.selectAdress_btnContinue);
        tvNewAdress=findViewById(R.id.selectAdress_tvNewAdress);
        setupRecycler();
        addNewAdress=new AddNewAdress();
        addNewAdress.setListener(this);

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),Thankyou.class);
                startActivity(intent);
            }
        });
        tvNewAdress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Toast.makeText(SelectAdress.this, "click", Toast.LENGTH_SHORT).show();
                Intent intent=new Intent(getApplicationContext(),AddNewAdress.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setupRecycler() {
        adressList.add(new dbAdress("Home","House # 123 Street 12 Rawalpindi"));
        adressList.add(new dbAdress("Shop","Shop # 123 Street 12 Rawalpindi"));
        adressList.add(new dbAdress("Office","Office # 123 Street 12 Rawalpindi"));

        rvAdress.setHasFixedSize(true);
        rvAdress.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rvAdress.setAdapter(new SelectAdressRecyler(this,adressList));

    }

    @Override
    public void onAdressEntered(String title, String Adress) {
        adressList.add(new dbAdress(title,Adress));
        rvAdress.setHasFixedSize(true);
        rvAdress.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rvAdress.setAdapter(new SelectAdressRecyler(this,adressList));
    }
}
