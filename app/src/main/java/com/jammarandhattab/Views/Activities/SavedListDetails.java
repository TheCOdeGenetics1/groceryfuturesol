package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.jammarandhattab.Views.Adapters.SavedListPreviewRecycler;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.Room.SavedList;

import java.util.ArrayList;
import java.util.List;

public class SavedListDetails extends AppCompatActivity  {
    RecyclerView rvbottomsheet;
    RelativeLayout rlTop;
    String title,date;
    List<SavedList> mData=new ArrayList<>();

    TextView tvTotalItemTop,tvTotalAmountTop,tvSubTotal,tvDiscount,tvDeliveryCharges,tvGrandTotal;
    Button btnSaveList,btnCheckout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_list_details);
        title=getIntent().getStringExtra("Title");
        date=getIntent().getStringExtra("Date");
        initComponents();
        initRecycler();
        clickEvents();


    }

    private void initComponents() {
        rvbottomsheet=findViewById(R.id.bottomSheet_rv);
        rlTop=findViewById(R.id.bottomSheetRLtop);

        tvTotalItemTop=findViewById(R.id.bottomsheet_tvTotalItemTop);
        tvTotalAmountTop=findViewById(R.id.bbottomsheet_tvTotalAmountTop);
        tvSubTotal=findViewById(R.id.bottomsheet_tvsubtotal);
        tvDiscount=findViewById(R.id.bottomsheet_tvDiscount);
        tvDeliveryCharges=findViewById(R.id.bottomsheet_tvDeliveryCharges);
        tvGrandTotal=findViewById(R.id.bottomsheet_tvGrandTotal);
        btnSaveList=findViewById(R.id.bottomsheet_btnSaveList);
        btnCheckout=findViewById(R.id.bottomsheet_btnCheckout);
    }

    private void initRecycler() {
        rlTop.setVisibility(View.GONE);
        mData=MainActivity.myAppDatabase.dao().getSavedListByTitleandDate(title,date);
        rvbottomsheet.setHasFixedSize(true);
        rvbottomsheet.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        rvbottomsheet.setAdapter(new SavedListPreviewRecycler(this,mData));
    }

    private void clickEvents()
    {
        btnSaveList.setText("Continue");
        btnSaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnSaveListOnClick();
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnCheckoutOnCLick();
            }
        });
    }

    public void btnCheckoutOnCLick()
    {

    }
    public void btnSaveListOnClick()
    {
        List<SavedList> savedList=new ArrayList<>();
        savedList=MainActivity.myAppDatabase.dao().getSavedListByTitleandDate(title,date);
        for(int i=0;i<savedList.size();i++)
        {
            Cart cart=new Cart();
            cart.setTitle(savedList.get(i).getItemName());
            cart.setOtherTitle(savedList.get(i).getUrduName());
            cart.setCategory(savedList.get(i).getCategory());
            cart.setPrice(String.valueOf(savedList.get(i).getPrice()));
            cart.setQuantity(String.valueOf(savedList.get(i).getQty()));
            cart.setImageUrl(savedList.get(i).getImage());

            MainActivity.myAppDatabase.dao().addCart(cart);

        }
        MainActivity.myAppDatabase.dao().deleteSavedListbyTitleandDate(title,date);
        startActivity(new Intent(SavedListDetails.this,MainActivity.class));
        finish();

    }


}
