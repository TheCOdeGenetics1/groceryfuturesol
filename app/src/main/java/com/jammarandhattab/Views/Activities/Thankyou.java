package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jammarandhattab.R;

public class Thankyou extends AppCompatActivity {
    Button btnContinue, btnClose;
LinearLayout bottomSheet;
BottomSheetBehavior sheetBehavior;
  //  CoordinatorLayout coordinatorLayout;

    ImageView btnExpand;

    TextView tvTotalItemTop,tvTotalAmountTop,tvSubTotal,tvDiscount,tvDeliveryCharges,tvGrandTotal;
    Button btnSaveList,btnCheckout;

    TextView tvDescription;
    String name,OrderID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thankyou);
        initComponents();
        clickEvents();
        name=getIntent().getStringExtra("Name");
        OrderID=getIntent().getStringExtra("OrderID");
        tvDescription.setText("Dear "+name+"\n Thank you for your order and you order number is "+OrderID+".\n We will contact you soon");
        MainActivity.myAppDatabase.dao().deleteCart();
    }

    private void clickEvents() {
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();


            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        btnExpand.setImageResource(R.drawable.ic_action_collapse);
                        // btnExpand.setRotation(180f);

                        // btnExpand.animate().rotationBy(180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
                       // showCartRecycler();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        btnExpand.setImageResource(R.drawable.ic_action_expand);

//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
//                        btnExpand.animate().rotationBy(-180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
                        //  btnExpand.setRotation(360f);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void initComponents() {
        //coordinatorLayout=findViewById(R.id.coordinator);
        btnContinue =findViewById(R.id.thankyou_btnContinue);
        btnClose =findViewById(R.id.thankyou_btnClose);
        bottomSheet=findViewById(R.id.subMenu_bottomSheet);
        sheetBehavior=BottomSheetBehavior.from(bottomSheet);
        btnExpand=findViewById(R.id.bottomsheet_imgexpand);
        sheetBehavior.setPeekHeight(0);
        tvDescription=findViewById(R.id.thankyou_tvDescription);

        tvTotalItemTop=findViewById(R.id.bottomsheet_tvTotalItemTop);
        tvTotalAmountTop=findViewById(R.id.bbottomsheet_tvTotalAmountTop);
        tvSubTotal=findViewById(R.id.bottomsheet_tvsubtotal);
        tvDiscount=findViewById(R.id.bottomsheet_tvDiscount);
        tvDeliveryCharges=findViewById(R.id.bottomsheet_tvDeliveryCharges);
        tvGrandTotal=findViewById(R.id.bottomsheet_tvGrandTotal);
        btnSaveList=findViewById(R.id.bottomsheet_btnSaveList);
        btnCheckout=findViewById(R.id.bottomsheet_btnCheckout);
    }
}
