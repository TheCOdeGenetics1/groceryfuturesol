package com.jammarandhattab.Views.Activities;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.jammarandhattab.Helper.MySingelton;
import com.jammarandhattab.R;
import com.jammarandhattab.Views.Adapters.OrderItemNewRecycler;
import com.jammarandhattab.Views.Adapters.OrderItemsRecycler;

public class OrderDetails extends AppCompatActivity {
    private TextView tvOrderID, tvOrderType, tvDishID, tvPaymentsMethod, tvOrderCode,
            tvCoupanCode, tvContactID, tvOrderStatus, tvTotalPrice, tvOrderFor,
            tvAddress, tvDateCreated, tvPaymentStatus;

    private RecyclerView rvOrderItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);
        initViews();
        setValues();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Order details");
        }

    }

    private void initViews() {
        tvOrderID = findViewById(R.id.orderDetail_tvOrderID);
        tvOrderType = findViewById(R.id.orderDetail_tvOrderType);
        tvDishID = findViewById(R.id.orderDetail_tvDishID);
        tvPaymentsMethod = findViewById(R.id.orderDetail_tvPaymentMethod);
        tvOrderCode = findViewById(R.id.orderDetail_tvOrderCode);
        tvCoupanCode = findViewById(R.id.orderDetail_tvCouponCode);
        tvContactID = findViewById(R.id.orderDetail_tvContactID);
        tvOrderStatus = findViewById(R.id.orderDetail_tvOrderStatus);
        tvTotalPrice = findViewById(R.id.orderDetail_tvTotalPrice);
        tvOrderFor = findViewById(R.id.orderDetail_tvOrderFor);
        tvAddress = findViewById(R.id.orderDetail_tvAddress);
        tvDateCreated = findViewById(R.id.orderDetail_tvDateCreated);
        tvPaymentStatus = findViewById(R.id.orderDetail_tvPaymentStatus);
        rvOrderItems = findViewById(R.id.orderDetail_rvOrderItems);
    }

    @SuppressLint("SetTextI18n")
    private void setValues() {
        try {
            tvOrderID.setText(String.valueOf(MySingelton.getInstance().getMyOrder().getOrderId()));
            tvOrderType.setText(MySingelton.getInstance().getMyOrder().getOrderType());
            tvDishID.setText(String.valueOf(MySingelton.getInstance().getMyOrder().getDishId()));
            tvPaymentsMethod.setText(MySingelton.getInstance().getMyOrder().getPaymentMethod());
            tvOrderCode.setText(MySingelton.getInstance().getMyOrder().getOrderCode());
            tvCoupanCode.setText(MySingelton.getInstance().getMyOrder().getCouponCode());
            tvContactID.setText(String.valueOf(MySingelton.getInstance().getMyOrder().getContactId()));
            tvOrderStatus.setText(MySingelton.getInstance().getMyOrder().getOrderStatus());
            tvTotalPrice.setText(String.valueOf(MySingelton.getInstance().getMyOrder().getTotalPrice()));
            tvOrderFor.setText(MySingelton.getInstance().getMyOrder().getOrderForFullName());
            tvAddress.setText(MySingelton.getInstance().getMyOrder().getOrderForAddressLine1() + "," + MySingelton.getInstance().getMyOrder().getOrderForAddressLine2());
            tvDateCreated.setText(MySingelton.getInstance().getMyOrder().getDateCreated());
            tvPaymentStatus.setText(MySingelton.getInstance().getMyOrder().getPaymentStatus());

            rvOrderItems.setHasFixedSize(true);
            rvOrderItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvOrderItems.setAdapter(new OrderItemNewRecycler(MySingelton.getInstance().getMyOrder().getOrderItems()));
        } catch (Exception e) {
            e.printStackTrace();
            String TAG = "Order Details";
            Log.e(TAG, "setValues: ", e.getCause());
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
