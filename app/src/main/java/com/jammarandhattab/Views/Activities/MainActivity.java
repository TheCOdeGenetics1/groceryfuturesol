package com.jammarandhattab.Views.Activities;

import android.arch.persistence.room.Room;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

//import com.amitshekhar.DebugDB;
import com.jammarandhattab.Helper.UserSession;
import com.jammarandhattab.Views.Fragments.Home;
import com.jammarandhattab.Views.Fragments.More;
import com.jammarandhattab.Views.Fragments.MyOrders;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.MyAppDatabase;

public class MainActivity extends AppCompatActivity {
    public static MyAppDatabase myAppDatabase;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            switch (item.getItemId()) {
                case R.id.menu_Home:
                    Home home=new Home();
                    fragmentTransaction.replace(R.id.content,home).addToBackStack(null).commit();
                    return true;
//                case R.id.menu_Orders:
//                    Orders orders=new Orders();
//                    fragmentTransaction.replace(R.id.content, orders).addToBackStack(null).commit();
//                    return true;
                case R.id.menu_MyOrders:
                    if (UserSession.getInstance(MainActivity.this).getMobile()!=null)
                    {
                        if (UserSession.getInstance(MainActivity.this).getMobile().isEmpty()){
                            startActivity(new Intent(MainActivity.this, LoginActivity.class));

                        }else {
                            MyOrders myOrders=new MyOrders();
                            fragmentTransaction.replace(R.id.content, myOrders).addToBackStack(null).commit();
                            return true;
                        }
                    }else {
                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    }
                    break;

//                case R.id.menu_SavedList:
//                    SavedList savedList=new SavedList();
//                    fragmentTransaction.replace(R.id.content, savedList).addToBackStack(null).commit();
//                    return true;
                case R.id.menu_More:
                    More more=new More();
                    fragmentTransaction.replace(R.id.content, more).addToBackStack(null).commit();
                    return true;
//
            }
            return false;
        }
    };

        @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        myAppDatabase= Room.databaseBuilder(getApplicationContext(),MyAppDatabase.class,"Storedb").allowMainThreadQueries().fallbackToDestructiveMigration().build();

            BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigationView);
            navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

            Home home=new Home();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.content, home).addToBackStack(null).commit();

//            MainActivity.myAppDatabase.dao().deleteCart();
//            MainActivity.myAppDatabase.dao().deleteCart();

//            DebugDB.getAddressLog();


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        myAppDatabase.dao().deleteCart();
    }
}
