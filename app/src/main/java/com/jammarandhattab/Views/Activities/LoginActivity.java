package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Presenters.LoginPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.Helper.UserSession;
import com.jammarandhattab.interfaces.ILogin;

import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements OnClickListener, ILogin.iMain {
    private  final String TAG = "Login Activity";
    EditText etMobile,etPassword;
    View viewSignUp,viewGuest;
    Button btnLogin;
    ProgressBar pb;
    LoginPresenter loginPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();

    }

    private void initViews() {
        etMobile=findViewById(R.id.login_etMobile);
        etPassword=findViewById(R.id.login_etPassword);
        viewSignUp=findViewById(R.id.login_viewSignUp);
        viewGuest=findViewById(R.id.login_viewGuest);
        btnLogin=findViewById(R.id.login_btnLogin);
        pb=findViewById(R.id.pb);
        loginPresenter=new LoginPresenter(this);
        viewSignUp.setOnClickListener(this);
        viewGuest.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    private boolean validateFileds(){
        if (etMobile.getText().toString().isEmpty()){
            etMobile.setError("Mobile No should'nt empty");
            return false;
        }else if(etPassword.getText().toString().isEmpty()){
            etPassword.setError("Password should'nt empty");
            return false;
        }
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_viewSignUp:
                startActivity(new Intent(LoginActivity.this,Registration.class));
                finish();
                break;
            case R.id.login_viewGuest:
                startActivity(new Intent(LoginActivity.this,MainActivity.class));
                finish();
                break;
            case R.id.login_btnLogin:
                if (validateFileds()){
                    loginPresenter.doLogin(etMobile.getText().toString(),etPassword.getText().toString(),1);
                }
                break;

        }
    }

    @Override
    public void showProgress() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        pb.setVisibility(View.GONE);
    }

    @Override
    public void setLoginResponse(Response<LoginResponse> response) {
        if (response.body()!=null){
            UserSession.getInstance(this).setMobile(response.body().getUserName());
            UserSession.getInstance(this).setCustomerID(String.valueOf(response.body().getCustomerID()));
            Toast.makeText(this, ""+response.body().getMessage(), Toast.LENGTH_SHORT).show();
            startActivity(new Intent(LoginActivity.this,MainActivity.class));
            finish();
        }
        else {
            Toast.makeText(this, "Invalid Mobile No/Password!!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void showError(Throwable t) {
        Log.e(TAG, "showError: ",t.getCause() );

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this,MainActivity.class));
        finish();
    }
}

