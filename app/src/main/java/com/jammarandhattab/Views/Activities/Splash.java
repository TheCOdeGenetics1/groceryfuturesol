package com.jammarandhattab.Views.Activities;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.jammarandhattab.Models.Catgories.Categories;
import com.jammarandhattab.Models.Menu.Menu;
import com.jammarandhattab.Models.photoSlider.PhotoSlider;
import com.jammarandhattab.Presenters.CategoryPresenter;
import com.jammarandhattab.Presenters.MenuPresenter;
import com.jammarandhattab.Presenters.PhotoSliderPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.Helper.MySingelton;
import com.jammarandhattab.interfaces.IMenu;
import com.jammarandhattab.interfaces.IPhotoSlider;
import com.jammarandhattab.interfaces.iCategory;

import retrofit2.Response;

public class Splash extends AppCompatActivity implements
        IMenu.iMain,
        iCategory.Main,
        IPhotoSlider.iMain {
    ProgressBar progressBar;
    MenuPresenter menuPresenter;
    CategoryPresenter categoryPresenter;
    PhotoSliderPresenter sliderPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        progressBar=findViewById(R.id.pbLoading);
        menuPresenter=new MenuPresenter(this);
        categoryPresenter=new CategoryPresenter(this);
        sliderPresenter=new PhotoSliderPresenter(this);
        categoryPresenter.getCategories();
        menuPresenter.getMenu();
        sliderPresenter.getSliderPhotos();



    }

    @Override
    public void showProgress() {
        progressBar.setProgress(50);
    }

    @Override
    public void setMenu(Response<Menu> response) {
        if (response.body()!=null){
            MySingelton.getInstance().setMenuList(response.body().getMenus());
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    startActivity(new Intent(Splash.this, MainActivity.class));
                    finish();



                }
            }, 400);
        }

    }

    @Override
    public void showError(Throwable t) {
//        Toast.makeText(getActivity(), "" + t, Toast.LENGTH_LONG).show();
        Snackbar.make(progressBar,"Something went wrong",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                categoryPresenter.getCategories();
                menuPresenter.getMenu();
            }
        }).show();
        Log.e("Home", "Error: " + t.getCause());
    }

    @Override
    public void setCategories(Response<Categories> response) {
        if (response.body()!=null){
            MySingelton.getInstance().setCategoryList(response.body().getMenuCategories());
        }
    }


    @Override
    public void hideProgress() {
        progressBar.setProgress(100);
    }

    @Override
    public void setPhotoData(Response<PhotoSlider> response) {
        if (response.body()!=null){
            MySingelton.getInstance().setSliderPhotos(response.body().getSlider().getPhotos());
        }
    }
}
