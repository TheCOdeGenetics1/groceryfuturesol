package com.jammarandhattab.Views.Adapters;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.jammarandhattab.Models.Catgories.MenuCategory;
import com.jammarandhattab.R;
import com.jammarandhattab.interfaces.IViewClicked;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;

public class CategoryRecycler extends RecyclerView.Adapter<CategoryRecycler.CategoryHolder> {

    @SuppressLint("StaticFieldLeak")
    public static ImageView imageView;
    public static int lastPosition;
    private List<MenuCategory> mData;

    public IViewClicked viewClicked;
    int mSelectedItem=-1;


    public CategoryRecycler(List<MenuCategory> mData, IViewClicked clicked) {
        this.viewClicked = clicked;
        this.mData = mData;

    }

    @NonNull
    @Override
    public CategoryHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CategoryHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.si_category, parent, false),viewClicked);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryHolder holder, int position) {

        final MenuCategory category = mData.get(holder.getAdapterPosition());

        holder.textView.setText(category.getCategoryTitle());
        imageView=holder.ivIndicator;

        if (position == mSelectedItem)
            holder.ivIndicator.setVisibility(View.VISIBLE);
        else holder.ivIndicator.setVisibility(View.INVISIBLE);

//        if (lastPosition == -1) category.setSelected(false);
//
//        if (category.isSelected())
//            holder.ivIndicator.setVisibility(View.VISIBLE);
//        else
//            holder.ivIndicator.setVisibility(View.INVISIBLE);

//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (imageView!=null){
//                    imageView.setVisibility(View.INVISIBLE);
//                    if (lastPosition != -1) mData.get(lastPosition).setSelected(false);
//                }
//                lastPosition = holder.getAdapterPosition();
//                imageView = holder.ivIndicator;
//                category.setSelected(true);
//                holder.ivIndicator.setVisibility(View.VISIBLE);
//                viewClicked.onClick(holder.getAdapterPosition());
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }



    class CategoryHolder extends RecyclerView.ViewHolder {
        private TextView textView;
        private ImageView ivIndicator;
        private IViewClicked viewClicked;

        CategoryHolder(View itemView, final IViewClicked iViewClicked) {
            super(itemView);
            this.viewClicked=iViewClicked;
            textView = itemView.findViewById(R.id.si_Category_tv);
            ivIndicator = itemView.findViewById(R.id.si_Category_indicator);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    Log.d("SelectAdressRecyler","adapterPosition"+getAdapterPosition());
                    Log.d("SelectAdressRecyler","Selected Item"+getAdapterPosition());
                    iViewClicked.onClick(getAdapterPosition());
                    notifyDataSetChanged();

                }
            });
        }

    }
}





