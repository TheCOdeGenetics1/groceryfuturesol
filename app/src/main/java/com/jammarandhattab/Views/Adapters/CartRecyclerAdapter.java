package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CartRecyclerAdapter extends RecyclerView.Adapter<CartRecyclerAdapter.CartHolder> {
    Context context;
    List<Cart> mData=new ArrayList<>();


    public CartRecyclerAdapter(Context context, List<Cart> mData) {
        this.context = context;
        this.mData = mData;
    }

    @Override
    public CartHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.si_cart,parent,false);

        return new CartHolder(view);
    }

    @Override
    public void onBindViewHolder(final CartHolder holder, int position) {
        Picasso.with(context).load(mData.get(holder.getAdapterPosition()).getImageUrl()).into(holder.img);
        holder.tvTitle.setText(mData.get(holder.getAdapterPosition()).getTitle());
        holder.tvUrdu.setText(mData.get(holder.getAdapterPosition()).getOtherTitle());
        holder.tvPrice.setText(String.valueOf("AED"+mData.get(holder.getAdapterPosition()).getPrice()));
        holder.tvQty.setText(String.valueOf(mData.get(holder.getAdapterPosition()).getQuantity()));

        holder.tvRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Remove called at"+holder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class CartHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView tvTitle,tvUrdu,tvPrice,tvQty,tvRemove;
        public CartHolder(View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.si_cart_img);
            tvTitle=itemView.findViewById(R.id.si_cart_tvTitle);
            tvUrdu=itemView.findViewById(R.id.si_cart_tvTitleUrdu);
            tvPrice=itemView.findViewById(R.id.si_cart_tvPrice);
            tvQty=itemView.findViewById(R.id.si_cart_tvQty);
            tvRemove=itemView.findViewById(R.id.si_Cart_tvRemove);
        }
    }
}
