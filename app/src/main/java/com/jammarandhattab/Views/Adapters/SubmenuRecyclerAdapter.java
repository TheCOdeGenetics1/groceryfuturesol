package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jammarandhattab.R;
import com.jammarandhattab.Utils.Common;

public class SubmenuRecyclerAdapter extends RecyclerView.Adapter<SubmenuRecyclerAdapter.SubmenuHolder> {

    Context context;
    ProductRecyclerAdapter.onQuantitychange onQuantitychange;


    public SubmenuRecyclerAdapter(Context context, ProductRecyclerAdapter.onQuantitychange onQuantitychange) {
        this.context = context;
        this.onQuantitychange = onQuantitychange;
    }

    @Override
    public SubmenuHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.si_submenu,parent,false);

        return new SubmenuHolder(view);
    }

    @Override
    public void onBindViewHolder(SubmenuHolder holder, int position) {

        holder.tvTitle.setText(Common.productList.get(position).getCategory());
        holder.rv.setHasFixedSize(true);
        holder.rv.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
//        holder.rv.setAdapter(new ProductRecyclerAdapter(context,Common.productList, onQuantitychange));

    }

    @Override
    public int getItemCount() {
        return Common.productList.size();
    }




    public class SubmenuHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        RecyclerView rv;
        public SubmenuHolder(View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.si_submenu_tvtitle);
            rv=itemView.findViewById(R.id.si_submenu_rv);
        }
    }
}
