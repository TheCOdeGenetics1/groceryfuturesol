package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jammarandhattab.Models.Catgories.MenuCategory;
import com.jammarandhattab.Models.Menu.MenuItems;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.Views.Activities.ItemPreview;
import com.jammarandhattab.Views.Activities.MainActivity;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Locale;

public class ProductRecyclerAdapter extends RecyclerView.Adapter<ProductRecyclerAdapter.ProductHolder> {

    private Context context;
    private List<MenuItems> mData;
    private List<MenuItems> arrayListFiltered = new ArrayList<>();


    public interface onQuantitychange {
        void onChangedQuantity();
    }

    private onQuantitychange onQuantitychange;


    public ProductRecyclerAdapter(Context context, List<MenuItems> mData, onQuantitychange onQuantitychange) {
        this.context = context;
        this.mData = mData;
        this.arrayListFiltered.addAll(mData);
        this.onQuantitychange = onQuantitychange;


    }

    @NotNull
    @Override
    public ProductHolder onCreateViewHolder(@NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.si_product_horizontal, parent, false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NotNull final ProductHolder holder, final int position) {

        List<Cart> list = MainActivity.myAppDatabase.dao()
                .getCartByPid(String.valueOf(mData.get(holder.getAdapterPosition()).getId()));
        if (list.size() > 0)
            if (Integer.parseInt(list.get(0).getQuantity()) > 0) {
                holder.btnAddToCart.setVisibility(View.GONE);
                holder.llPlusMinus.setVisibility(View.VISIBLE);
                holder.tvQty.setText(String.valueOf(list.get(0).getQuantity()));
            }

        holder.tvTitle.setText(mData.get(holder.getAdapterPosition()).getTitle());
        holder.tvPrice.setText(String.format(Locale.ENGLISH, "AED %d", mData.get(holder.getAdapterPosition()).getPrice()));
        holder.tvTitleU.setText(mData.get(holder.getAdapterPosition()).getOtherTitle());

        Picasso.with(context)
                .load(mData.get(holder.getAdapterPosition()).getThumbnailUrl())
                .placeholder(R.drawable.loading_placeholder)
//                .resize(100,100)
                .error(R.drawable.fail_placeholder)
                .into(holder.img);


        list.clear();


        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.btnAddToCart.setVisibility(View.GONE);
                holder.llPlusMinus.setVisibility(View.VISIBLE);
                holder.count++;
                holder.tvQty.setText(String.valueOf(holder.count));

                Cart cart = new Cart();
                cart.setP_Id(String.valueOf(mData.get(holder.getAdapterPosition()).getId()));
                cart.setCategory(mData.get(holder.getAdapterPosition()).getCategory());
                cart.setTitle(mData.get(holder.getAdapterPosition()).getTitle());
                cart.setOtherTitle(mData.get(holder.getAdapterPosition()).getOtherTitle());
                cart.setQuantity(String.valueOf(holder.count));
                cart.setPrice((String.valueOf(mData.get(holder.getAdapterPosition()).getPrice())));
                cart.setImageUrl(mData.get(holder.getAdapterPosition()).getThumbnailUrl());
                MainActivity.myAppDatabase.dao().addCart(cart);

                List<Cart> temolist = MainActivity.myAppDatabase.dao().getCart();

                Log.i("ProductRecyclerAdapter", "List" + temolist.get(0).getTitle());
                onQuantitychange.onChangedQuantity();


            }
        });

        holder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.count = Integer.parseInt(holder.tvQty.getText().toString());
                holder.count++;
                holder.tvQty.setText(String.valueOf(holder.count));


                Cart cart = new Cart();
                cart.setCategory(mData.get(holder.getAdapterPosition()).getCategory());
                cart.setTitle(mData.get(holder.getAdapterPosition()).getTitle());
                cart.setOtherTitle(mData.get(holder.getAdapterPosition()).getOtherTitle());
                cart.setQuantity(String.valueOf(holder.count));
                cart.setPrice((String.valueOf(mData.get(holder.getAdapterPosition()).getPrice())));
                cart.setImageUrl(mData.get(holder.getAdapterPosition()).getThumbnailUrl());
                MainActivity.myAppDatabase.dao().updatecart(holder.count, String.valueOf(mData.get(holder.getAdapterPosition()).getId()));

                onQuantitychange.onChangedQuantity();


            }
        });

        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.count = Integer.parseInt(holder.tvQty.getText().toString());
                holder.count--;
                if (holder.count > 0) {
                    holder.tvQty.setText(String.valueOf(holder.count));

                    Cart cart = new Cart();
                    cart.setCategory(mData.get(holder.getAdapterPosition()).getCategory());
                    cart.setTitle(mData.get(holder.getAdapterPosition()).getTitle());
                    cart.setOtherTitle(mData.get(holder.getAdapterPosition()).getOtherTitle());
                    cart.setQuantity(String.valueOf(holder.count));
                    cart.setPrice((String.valueOf(mData.get(holder.getAdapterPosition()).getPrice())));
                    cart.setImageUrl(mData.get(holder.getAdapterPosition()).getThumbnailUrl());
                    MainActivity.myAppDatabase.dao().updatecart(holder.count, String.valueOf(mData.get(holder.getAdapterPosition()).getId()));
                    onQuantitychange.onChangedQuantity();
                } else {

                    holder.llPlusMinus.setVisibility(View.GONE);
                    holder.btnAddToCart.setVisibility(View.VISIBLE);


                    Cart cart = new Cart();
                    cart.setCategory(mData.get(holder.getAdapterPosition()).getCategory());
                    cart.setTitle(mData.get(holder.getAdapterPosition()).getTitle());
                    cart.setOtherTitle(mData.get(holder.getAdapterPosition()).getOtherTitle());
                    cart.setQuantity(String.valueOf(holder.count));
                    cart.setPrice((String.valueOf(mData.get(holder.getAdapterPosition()).getPrice())));
                    cart.setImageUrl(mData.get(holder.getAdapterPosition()).getThumbnailUrl());
                    MainActivity.myAppDatabase.dao().deleteCartByPid(String.valueOf(mData.get(holder.getAdapterPosition()).getId()));
                    onQuantitychange.onChangedQuantity();


                }
                onQuantitychange.onChangedQuantity();

            }
        });

//        holder.img
//                .setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        Intent intent = new Intent(context, ItemPreview.class);
//                        context.startActivity(intent);
//                    }
//                });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void filter(String search) {

        if (mData != null) {
            mData.clear();
            if (search.length() == 0) {
                mData.addAll(arrayListFiltered);
            } else {
                for (MenuItems items : arrayListFiltered) {

                    if (items.getTitle().toLowerCase(Locale.getDefault()).contains(search)) {
                        mData.add(items);
                    }
                }
            }
            notifyDataSetChanged();
        }
    }

    public void update(List<MenuItems> menuItemsList) {
        mData.clear();
        arrayListFiltered.clear();
        mData.addAll(menuItemsList);
        arrayListFiltered.addAll(mData);
        notifyDataSetChanged();
    }


    class ProductHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvPrice, tvQty, tvTitleU;
        Button btnAddToCart, btnPlus, btnMinus;
        ImageView img;
        LinearLayout llPlusMinus;

        int count = 0;

        public ProductHolder(View itemView) {
            super(itemView);

//            tvTitle=itemView.findViewById(R.id.si_product_tvTitle);
//            tvPrice=itemView.findViewById(R.id.si_product_tvPrice);
//            btnAddToCart=itemView.findViewById(R.id.si_product_btnAddtoCart);
//            img=itemView.findViewById(R.id.si_product_img);

            tvTitle = itemView.findViewById(R.id.si_product_tvTitle_h);
            tvPrice = itemView.findViewById(R.id.si_product_tvPrice_h);
            btnAddToCart = itemView.findViewById(R.id.si_product_btnAddtoCart_h);
            img = itemView.findViewById(R.id.si_product_img_h);
            llPlusMinus = itemView.findViewById(R.id.si_product_llPlusMinus_h);
            tvQty = itemView.findViewById(R.id.si_product_tvQty_h);
            btnPlus = itemView.findViewById(R.id.si_product_btnPlus_h);
            btnMinus = itemView.findViewById(R.id.si_product_btnMinus_h);
            tvTitleU = itemView.findViewById(R.id.si_product_tvTitleUrdu_h);


        }
    }
}


//    private Filter filter = new Filter() {
//
//        @Override
//        protected FilterResults performFiltering(CharSequence constraint) {
//
//            if (constraint != null) {
//                arrayListFiltered.clear();
//
//                for (MenuItems item : mData) {
//                    String title = item.getTitle();
//                    if (item.getTitle().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
//                        arrayListFiltered.add(item);
//                    }
//                }
//
//                //Collections.sort(arrayListFiltered, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
//
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = arrayListFiltered;
//                filterResults.count = arrayListFiltered.size();
//                return filterResults;
//            } else {
//                return new FilterResults();
//            }
//        }
//
//        @Override
//        protected void publishResults(CharSequence constraint, FilterResults results) {
//            try {
//                if (results != null && results.count > 0) {
//                    arrayListFiltered = (ArrayList<MenuItems>) results.values;
//                    notifyDataSetChanged();
//                }
//            } catch (ConcurrentModificationException e) {
//                Log.e("ADAPTER", "CRASHED: " + e.getLocalizedMessage());
//            }
//        }
//
//    };


