package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jammarandhattab.Views.Fragments.dbAdress;
import com.jammarandhattab.R;

import java.util.ArrayList;
import java.util.List;

public class SelectAdressRecyler extends RecyclerView.Adapter<SelectAdressRecyler.SelectAdressHolder> {
    Context context;
    List<dbAdress> mData=new ArrayList<>();
    int mSelectedItem=-1;


    public SelectAdressRecyler(Context context, List<dbAdress> mData) {
        this.context = context;
        this.mData = mData;

    }

    @NonNull
    @Override
    public SelectAdressHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.si_selectadress,parent,false);

        return new SelectAdressHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectAdressHolder holder, final int position) {
        holder.imgCheck.setVisibility(View.GONE);
        holder.tvTitle.setText(mData.get(position).getTitle());
        holder.tvAdress.setText(mData.get(position).getAdress());

        if (position == mSelectedItem)
            holder.imgCheck.setVisibility(View.VISIBLE);
        else holder.imgCheck.setVisibility(View.GONE);

        Log.d("Select Adress Recyler","adapterPosition"+holder.getAdapterPosition());

//        holder.cvTop.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View view) {
//
////                if (holder.imgCheck.getVisibility()==View.VISIBLE)
////                    holder.imgCheck.setVisibility(View.GONE);
////                else
////                    holder.imgCheck.setVisibility(View.VISIBLE);
//
//
////               if (!holder.isChecked)
////               {
////                   holder.imgCheck.setVisibility(View.VISIBLE);
////                   holder.isChecked=true;
////               }else {
////                   holder.imgCheck.setVisibility(View.GONE);
////                   holder.isChecked=false;
//
//
////               }
//
//
//              // notifyDataSetChanged();
//                Log.d("Item","ItemID "+getItemId(position));
//                Log.d("Item","ItemCount "+getItemCount());
//                Log.d("Item","ItemViewType"+getItemViewType(position));
//                Log.d("Item","OLdPOsition"+holder.getOldPosition());
//                Log.d("Item","New POsition\""+holder.getAdapterPosition());
//
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SelectAdressHolder extends RecyclerView.ViewHolder {
        boolean isChecked=false;
        CardView cvTop;
        TextView tvTitle,tvAdress;
        ImageView imgCheck;
        public SelectAdressHolder(View itemView) {
            super(itemView);
            cvTop=itemView.findViewById(R.id.si_selectAdress_cvTop);
            tvTitle =itemView.findViewById(R.id.si_selectAdress_tvTitle);
            tvAdress=itemView.findViewById(R.id.si_selectAdress_tvAdress);
            imgCheck=itemView.findViewById(R.id.si_selectAdress_imgCheck);
            cvTop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSelectedItem = getAdapterPosition();
                    Log.d("SelectAdressRecyler","adapterPosition"+getAdapterPosition());
                    Log.d("SelectAdressRecyler","Selected Item"+getAdapterPosition());
                    notifyDataSetChanged();

                }
            });

        }
    }
}
