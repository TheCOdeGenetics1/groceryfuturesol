package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jammarandhattab.R;
import com.jammarandhattab.Room.SavedList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class SavedListPreviewRecycler extends RecyclerView.Adapter<SavedListPreviewRecycler.SavedListPreviewHolder> {
    Context context;
    List<SavedList> mData =new ArrayList<>();

    public SavedListPreviewRecycler(Context context, List<SavedList> mData) {
        this.context = context;
        this.mData = mData;
    }

    @Override
    public SavedListPreviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.si_cart,parent,false);
        return new SavedListPreviewHolder(view);
    }

    @Override
    public void onBindViewHolder(SavedListPreviewHolder holder, int position) {
        Picasso.with(context).load(mData.get(holder.getAdapterPosition()).getImage()).into(holder.img);
        holder.tvTitle.setText(mData.get(holder.getAdapterPosition()).getItemName());
        holder.tvUrdu.setText(mData.get(holder.getAdapterPosition()).getUrduName());
        holder.tvPrice.setText(String.valueOf(mData.get(holder.getAdapterPosition()).getPrice()));
        holder.tvQty.setText(String.valueOf(mData.get(holder.getAdapterPosition()).getQty()));

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SavedListPreviewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle,tvUrdu,tvQty,tvPrice;
        ImageView img;

        public SavedListPreviewHolder(View itemView) {
            super(itemView);
            img=itemView.findViewById(R.id.si_cart_img);
            tvTitle=itemView.findViewById(R.id.si_cart_tvTitle);
            tvUrdu=itemView.findViewById(R.id.si_cart_tvTitleUrdu);
            tvPrice=itemView.findViewById(R.id.si_cart_tvPrice);
            tvQty=itemView.findViewById(R.id.si_cart_tvQty);

        }
    }
}
