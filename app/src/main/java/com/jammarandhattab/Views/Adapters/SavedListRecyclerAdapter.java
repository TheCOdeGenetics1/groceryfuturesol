package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.jammarandhattab.Models.dbListTitle;
import com.jammarandhattab.Views.Activities.SavedListDetails;
import com.jammarandhattab.R;

import java.util.ArrayList;
import java.util.List;

public class SavedListRecyclerAdapter extends RecyclerView.Adapter<SavedListRecyclerAdapter.SavedListHolder> {
    Context context;
    List<dbListTitle> mDate=new ArrayList<>();

    public SavedListRecyclerAdapter(Context context, List<dbListTitle> mDate) {
        this.context = context;
        this.mDate = mDate;
    }

    @Override
    public SavedListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.si_savelist,parent,false);
        return new SavedListHolder(view);
    }

    @Override
    public void onBindViewHolder(final SavedListHolder holder, int position) {
        holder.tvTitle.setText(mDate.get(position).getListTitle());
        holder.tvDate.setText(mDate.get(position).getDate());
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context, SavedListDetails.class);
                intent.putExtra("Title",holder.tvTitle.getText());
                intent.putExtra("Date",holder.tvDate.getText());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDate.size();
    }

    public class SavedListHolder extends RecyclerView.ViewHolder {
        TextView tvTitle,tvItems,tvDate;
        MaterialRippleLayout cv;
        public SavedListHolder(View itemView) {
            super(itemView);
            tvTitle=itemView.findViewById(R.id.savedList_tvTitle);
         //   tvItems=itemView.findViewById(R.id.savedList_tvItems);
            tvDate=itemView.findViewById(R.id.savedList_tvDate);
            cv=itemView.findViewById(R.id.si_savelist_Cardview);
        }
    }
}
