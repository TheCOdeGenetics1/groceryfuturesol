package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jammarandhattab.Models.photoSlider.Photo;
import com.jammarandhattab.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageSliderAdapter extends PagerAdapter {

    private Context mContext;
    private List<Photo> imageList;
    private LayoutInflater inflater;

    public ImageSliderAdapter(Context context, List<Photo> list) {
        mContext = context;
        imageList = list;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        ViewGroup imageLayout = (ViewGroup) inflater.inflate(R.layout.si_slidingimage_layout, collection, false);

        ImageView imageView= imageLayout.findViewById(R.id.imageview);
        try{
            Picasso.with(mContext).load(imageList.get(position).getPhotoUrl())
                    .placeholder(R.drawable.slider_loader).into(imageView);
        }catch (Exception e)
        {
            Log.e("IMage SLider Adapter :",e.getMessage());
        }

       // imageView.setImage(ImageSource.uri( Common.PhotoURl+imageList.get(position).getPhotoSource()));

        collection.addView(imageLayout);
        return imageLayout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        //return CustomPagerEnum.values().length;
        return imageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return imageList.get(position).toString();
    }
}
