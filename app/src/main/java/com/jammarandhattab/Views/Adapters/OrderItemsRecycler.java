package com.jammarandhattab.Views.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jammarandhattab.Models.MyOrders.OrderItem;
import com.jammarandhattab.R;

import java.util.List;

import static android.content.ContentValues.TAG;

public class OrderItemsRecycler extends RecyclerView.Adapter<OrderItemsRecycler.OrderItemHolder> {
    private List<OrderItem> mData;

    public OrderItemsRecycler(List<OrderItem> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public OrderItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.si_order_items, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemHolder holder, int position) {

        try {
            OrderItem orderItem;
            orderItem=mData.get(holder.getAdapterPosition());

            holder.tvOrderItemID.setText(String.valueOf(orderItem.getOrderItemId()));
            holder.tvQunattity.setText(String.valueOf(orderItem.getQty()));
            holder.tvNetTotal.setText(String.valueOf(orderItem.getNetTotal()));
            holder.tvNetWeight.setText(String.valueOf(orderItem.getNetWeight()));
            holder.tvUnitPrice.setText(String.valueOf(orderItem.getUnitprice()));
            holder.tvORderID.setText(String.valueOf(orderItem.getOrderId()));
            holder.tvProductID.setText(String.valueOf(orderItem.getProductId()));

        }catch (Exception e){
            e.printStackTrace();
            Log.e(TAG, "onBindViewHolder: ",e.getCause() );
        }

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class OrderItemHolder extends RecyclerView.ViewHolder {
        TextView tvOrderItemID,tvQunattity,tvNetTotal,tvNetWeight,tvUnitPrice,tvORderID,tvProductID;

        public OrderItemHolder(View itemView) {
            super(itemView);
            tvOrderItemID=itemView.findViewById(R.id.si_orderItems_tvOrderItemID);
            tvQunattity=itemView.findViewById(R.id.si_orderItems_tvQuantity);
            tvNetTotal=itemView.findViewById(R.id.si_orderItems_tvNetTotal);
            tvNetWeight=itemView.findViewById(R.id.si_orderItems_tvNetWeight);
            tvUnitPrice=itemView.findViewById(R.id.si_orderItems_tvUnitPrice);
            tvORderID=itemView.findViewById(R.id.si_orderItems_tvOrderID);
            tvProductID=itemView.findViewById(R.id.si_orderItems_tvProductID);
        }
    }
}
