package com.jammarandhattab.Views.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jammarandhattab.Models.MyOrders.Menu;
import com.jammarandhattab.Models.MyOrders.MyOrder;
import com.jammarandhattab.Models.dbListTitle;
import com.jammarandhattab.R;

import java.util.ArrayList;
import java.util.List;

public class MyOrdersRecyler extends RecyclerView.Adapter<MyOrdersRecyler.MyOrdersHolder> {
    Context context;
    List<Menu>mData=new ArrayList<>();

    public MyOrdersRecyler(Context context, List<Menu> mData) {
        this.context = context;
        this.mData = mData;
    }

    @NonNull
    @Override
    public MyOrdersHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(R.layout.si_myorders,parent,false);
        return new MyOrdersHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyOrdersHolder holder, int position) {
        if (mData.get(position).getPaymentStatus().contains("Pending"))
        {
           // holder.tvDate.setTextColor(Color.RED);
            holder.ll.setBackgroundResource(R.color.TransRed);
        }else {
            //holder.tvDate.setTextColor(Color.GREEN);
            holder.ll.setBackgroundResource(R.color.TransGreen);
        }
        holder.tvDate.setText(mData.get(position).getDateCreated());
        holder.tvItems.setText(mData.get(position).getOrderForFullName());

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class MyOrdersHolder extends RecyclerView.ViewHolder {
        TextView tvDate,tvItems;
        LinearLayout ll;
        public MyOrdersHolder(View itemView) {
            super(itemView);
            tvDate=itemView.findViewById(R.id.si_myOrders_tvDate);
            tvItems=itemView.findViewById(R.id.si_myOrders_tvItems);
            ll=itemView.findViewById(R.id.lyt_parent);
        }
    }
}
