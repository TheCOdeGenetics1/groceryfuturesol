package com.jammarandhattab.Views.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jammarandhattab.Models.MyOrders.OrderItem;
import com.jammarandhattab.R;

import java.util.List;

public class OrderItemNewRecycler extends RecyclerView.Adapter<OrderItemNewRecycler.OrderItemHolder> {
    private List<OrderItem> mData;

    public OrderItemNewRecycler(List<OrderItem> mData) {
        this.mData = mData;
    }

    @NonNull
    @Override
    public OrderItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new OrderItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.si_order_items_new,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull OrderItemHolder holder, int position) {
        OrderItem orderItem=mData.get(holder.getAdapterPosition());
        holder.tvSr.setText(String.valueOf(position+1));
        holder.tvName.setText(String.valueOf(orderItem.getProductId()));
        holder.tvRate.setText(String.valueOf(orderItem.getUnitprice()));
        holder.tvQuantity.setText(String.valueOf(orderItem.getQty()));
        holder.tvTotal.setText(String.valueOf(orderItem.getNetTotal()));

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class OrderItemHolder extends RecyclerView.ViewHolder {
        TextView tvSr,tvName,tvRate,tvQuantity,tvTotal;
        public OrderItemHolder(View itemView) {
            super(itemView);
            tvSr=itemView.findViewById(R.id.si_orderItem_tvSr);
            tvName=itemView.findViewById(R.id.si_orderItem_tvProductName);
            tvRate=itemView.findViewById(R.id.si_orderItem_tvRate);
            tvQuantity=itemView.findViewById(R.id.si_orderItem_tvQuantity);
            tvTotal=itemView.findViewById(R.id.si_orderItem_tvTotal);
        }
    }
}
