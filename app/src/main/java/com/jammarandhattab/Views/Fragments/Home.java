package com.jammarandhattab.Views.Fragments;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jammarandhattab.Models.Catgories.MenuCategory;
import com.jammarandhattab.Models.Menu.Menu;
import com.jammarandhattab.Models.Menu.MenuItems;
import com.jammarandhattab.Presenters.CategoryPresenter;
import com.jammarandhattab.Presenters.MenuPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.Room.Cart;
import com.jammarandhattab.Helper.MySingelton;
import com.jammarandhattab.Views.Activities.CheckOut;
import com.jammarandhattab.Views.Activities.MainActivity;
import com.jammarandhattab.Views.Adapters.CartRecyclerAdapter;
import com.jammarandhattab.Views.Adapters.CategoryRecycler;
import com.jammarandhattab.Views.Adapters.ProductRecyclerAdapter;
import com.jammarandhattab.interfaces.IMenu;
import com.jammarandhattab.interfaces.IViewClicked;
import com.jammarandhattab.Views.Adapters.ImageSliderAdapter;
import com.jammarandhattab.Utils.Common;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;
import com.tuyenmonkey.mkloader.MKLoader;
import com.viewpagerindicator.CirclePageIndicator;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class Home extends Fragment implements
//        iCategory.Main,
        IMenu.iMain,
        IViewClicked,
        ProductRecyclerAdapter.onQuantitychange {

    ViewPager viewPager;
    int currentPage = 0;
    int NUM_PAGES = 0;
    CirclePageIndicator indicator;
    ProgressBar progressBar;
    MKLoader sliderLoader;
    RelativeLayout rlFreshFood;
    CategoryPresenter categoryPresenter;
    MenuPresenter menuPresenter;
    RecyclerView rvCategory, rvMenu;
    CategoryRecycler categoryAdapter;
    ProductRecyclerAdapter productRecyclerAdapter;
    List<MenuCategory> categoryList = new ArrayList<>();
    List<MenuItems> menuItemsList;

    TextView tvAll;

    private EditText etSearch;
    private TextView tvTitle;
    private ImageView ivTabIndicator;
    private RelativeLayout rlAllIndicator;

    //Bottom sheet
    LinearLayout bottomSheetLayout;
    BottomSheetBehavior bottomSheetBehavior;
    ImageView btnExpand;
    RecyclerView rvbottomsheet;
    RelativeLayout rlTop;
    CoordinatorLayout coordinatorLayout;

    View viewBottomSheet,viewMain;

    TextView tvTotalItemTop,tvTotalAmountTop,tvSubTotal,tvDiscount,tvDeliveryCharges,tvGrandTotal;
    Button btnSaveList,btnCheckout,viewSplash;

    SweetAlertDialog progressDialog;


    public Home() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        initViews(v);

        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

        initImageSlider();
//        showProgress();



//        try {
//            new Thread(new Runnable() {
//
//                @Override
//                public void run() {
////                    showProgress();
        setCategoryFromSingelton();
        menuPresenter.getMenu();
//                    setMenuFromSingelton();

//                }
//            }).start();
//        } catch (Exception e){
//            e.printStackTrace();
//        }

//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//
//                hideProgress();
//
//
//            }
//        }, 1000);









        return v;
    }

    private void initViews(View v) {
        indicator = v.findViewById(R.id.home_indicator);
        viewPager = v.findViewById(R.id.home_viewPager);
        rlFreshFood = v.findViewById(R.id.home_rlFreshFood);
        rvMenu = v.findViewById(R.id.home_rvMenu);
        rvCategory = v.findViewById(R.id.home_rvCategory);
        progressBar = v.findViewById(R.id.pb);
        tvAll = v.findViewById(R.id.home_tvAll);

        etSearch = v.findViewById(R.id.home_etSearch);
        tvTitle = v.findViewById(R.id.home_tvTitle);
        ivTabIndicator = v.findViewById(R.id.home_tvAllIndicator);
        rlAllIndicator = v.findViewById(R.id.home_rlAllIndicator);

        //Bottom sheet
        btnExpand=v.findViewById(R.id.bottomsheet_imgexpand);
        bottomSheetLayout=v.findViewById(R.id.subMenu_bottomSheet);
        coordinatorLayout=v.findViewById(R.id.coordinator);
        rlTop=v.findViewById(R.id.bottomSheetRLtop);
        bottomSheetBehavior=BottomSheetBehavior.from(bottomSheetLayout);
        bottomSheetLayout.setBackgroundColor(getResources().getColor(R.color.white));
        rvbottomsheet=v.findViewById(R.id.bottomSheet_rv);
        tvTotalItemTop=v.findViewById(R.id.bottomsheet_tvTotalItemTop);
        tvTotalAmountTop=v.findViewById(R.id.bbottomsheet_tvTotalAmountTop);
        tvSubTotal=v.findViewById(R.id.bottomsheet_tvsubtotal);
        tvDiscount=v.findViewById(R.id.bottomsheet_tvDiscount);
        tvDeliveryCharges=v.findViewById(R.id.bottomsheet_tvDeliveryCharges);
        tvGrandTotal=v.findViewById(R.id.bottomsheet_tvGrandTotal);
        btnSaveList=v.findViewById(R.id.bottomsheet_btnSaveList);
        btnCheckout=v.findViewById(R.id.bottomsheet_btnCheckout);
        sliderLoader=v.findViewById(R.id.home_sliderMkLoader);

//        viewBottomSheet=v.findViewById(R.id.home_bottomSheet);
//        viewSplash=v.findViewById(R.id.home_splash);
        viewMain=v.findViewById(R.id.home_main);
//        viewMain.setVisibility(View.GONE);
        menuItemsList = new ArrayList<>();



//        categoryPresenter = new CategoryPresenter(this);
        menuPresenter = new MenuPresenter(this);



        progressDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.PROGRESS_TYPE);
        progressDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        progressDialog.setTitleText("Loading");
        progressDialog.setCancelable(false);


        clickEvents();


    }

    private void clickEvents() {
        btnExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (bottomSheetBehavior.getState()!=BottomSheetBehavior.STATE_EXPANDED)
                {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                }else {
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    btnExpand.setImageResource(R.drawable.ic_action_collapse);
                    coordinatorLayout.setVisibility(View.INVISIBLE);
                    coordinatorLayout.setVisibility(View.VISIBLE);

                }
            }



        });

        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState)
                {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        btnExpand.setImageResource(R.drawable.ic_action_collapse);
                        // btnExpand.setRotation(180f);

                        // btnExpand.animate().rotationBy(180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
//                        coordinatorLayout.setVisibility(View.INVISIBLE);
//                        coordinatorLayout.setVisibility(View.VISIBLE);
                        showCartRecycler();
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        btnExpand.setImageResource(R.drawable.ic_action_expand);

                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
//                        btnExpand.animate().rotationBy(-180f).setDuration(300).setInterpolator(new LinearInterpolator()).start();
                        //  btnExpand.setRotation(360f);
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
//                        btnExpand.animate().rotationBy(180).setDuration(1000).setInterpolator(new LinearInterpolator()).start();
                        coordinatorLayout.setVisibility(View.INVISIBLE);
                        coordinatorLayout.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {


            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getActivity(), CheckOut.class);
                intent.putExtra("TotalPrice",tvGrandTotal.getText());
                startActivity(intent);
                getActivity().finish();
            }
        });

        btnSaveList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnExpand.performClick();
            }
        });

        tvAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                rvCategory.setVisibility(View.INVISIBLE);
//                rlAllIndicator.setVisibility(View.GONE);
                ivTabIndicator.setVisibility(View.VISIBLE);
                tvTitle.setText("All");
                rvMenu.setVisibility(View.INVISIBLE);
                if (CategoryRecycler.imageView!=null){
//                    CategoryRecycler.lastPosition = -1;
                    CategoryRecycler.imageView.setVisibility(View.INVISIBLE);
                }
                menuPresenter.getMenu();
//                setMenuFromSingelton();
//                showProgress();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//
////               hideProgress();
//                    }
//                }, 500);

            }
        });



    }

    private void initImageSlider() {
//        Common.sliderImages.clear();
//        Common.sliderImages.add(R.drawable.slider_a);
//        Common.sliderImages.add(R.drawable.slider_b);

        viewPager.setAdapter(new ImageSliderAdapter(getActivity(), MySingelton.getInstance().getSliderPhotos()));

        indicator.setViewPager(viewPager);

        final float density = getResources().getDisplayMetrics().density;

        //Set circle indicator radius
        indicator.setRadius(3 * density);

        NUM_PAGES = Common.sliderImages.size();

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);

            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 3000, 3000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
//        categoryPresenter.getCategories();
//          getMenuFromSingelton();
//        menuPresenter.getMyOrders();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
//        viewMain.setVisibility(View.GONE);
//        progressDialog.show();
    }

    @Override
    public void showError(Throwable t) {
//        Toast.makeText(getActivity(), "" + t, Toast.LENGTH_LONG).show();
        Snackbar.make(coordinatorLayout,"Error in loading data",Snackbar.LENGTH_INDEFINITE).setAction("Retry", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                setMenuFromSingelton();
                menuPresenter.getMenu();
            }
        }).show();
        Log.e("Home", "Error: " + t.getCause());
    }

    @Override
    public void setMenu(Response<Menu> response) {
        if (response.body() != null) {

            menuItemsList.clear();
            menuItemsList = response.body().getMenus();
//            productRecyclerAdapter.update(menuItemsList);
            productRecyclerAdapter = new ProductRecyclerAdapter(getActivity(), menuItemsList, this);
            rvMenu.setLayoutManager(new LinearLayoutManager(getActivity()));
            rvMenu.setAdapter(productRecyclerAdapter);
            rlAllIndicator.setVisibility(View.VISIBLE);
            rvCategory.setVisibility(View.VISIBLE);
            rvMenu.setVisibility(View.VISIBLE);

            viewMain.setVisibility(View.VISIBLE);
//            hideProgress();
            showCartRecycler();


        } else {
            Toast.makeText(getActivity(), "No items found", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void hideProgress() {
//        progressDialog.dismiss();
        progressBar.setVisibility(View.GONE);
    }



    @Override
    public void onChangedQuantity() {
        showCartRecycler();
    }

    public  void showCartRecycler() {
        double totalAmount = 0;
        int totalItems = 0;
        CartRecyclerAdapter adapter;

        List<Cart> Cartlist= MainActivity.myAppDatabase.dao().getCart();
        if (Cartlist.size()>0) {
            rvbottomsheet.setHasFixedSize(true);
            rvbottomsheet.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            rvbottomsheet.setAdapter(new CartRecyclerAdapter(getActivity(), Cartlist));
        }else {
            rvbottomsheet.setHasFixedSize(true);
            rvbottomsheet.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
            rvbottomsheet.setAdapter(new CartRecyclerAdapter(getActivity(), Cartlist));
        }

        for (int i=0;i<Cartlist.size();i++)
        {
            totalItems=totalItems+Integer.parseInt(Cartlist.get(i).getQuantity());
            totalAmount=totalAmount+(Double.parseDouble(Cartlist.get(i).getQuantity())*Double.parseDouble(Cartlist.get(i).getPrice()));
        }

        tvTotalItemTop.setText(String.valueOf(totalItems));
        tvSubTotal.setText("AED "+String.valueOf(totalAmount));
        tvTotalAmountTop.setText("AED "+String.valueOf(totalAmount));
        double discount=0,  dileveryCharges=0;

        Double grandTotal=totalAmount+discount+dileveryCharges;

        tvGrandTotal.setText("AED "+grandTotal);



    }

    @Override
    public void onClick(int position) {
        rvCategory.setEnabled(false);
        ivTabIndicator.setVisibility(View.INVISIBLE);
        rvMenu.setVisibility(View.INVISIBLE);
        tvTitle.setText(categoryList.get(position).getCategoryTitle());
        String a = categoryList.get(position).getCategorySlug();
        menuPresenter.getMenu(1, categoryList.get(position).getCategorySlug());
    }

    private void setCategoryFromSingelton(){
        rvCategory.setEnabled(true);
        categoryList =MySingelton.getInstance().getCategoryList();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                rlAllIndicator.setVisibility(View.VISIBLE);
                rvCategory.setVisibility(View.VISIBLE);
                tvTitle.setText("All");
            }
        });

//            viewSplash.setVisibility(View.GONE);
//            viewMain.setVisibility(View.VISIBLE);
//            viewBottomSheet.setVisibility(View.VISIBLE);

        categoryAdapter = new CategoryRecycler(MySingelton.getInstance().getCategoryList(), this);
        rvCategory.setHasFixedSize(true);
        rvCategory.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvCategory.setAdapter(categoryAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                productRecyclerAdapter.filter(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setMenuFromSingelton(){

        if (MySingelton.getInstance().getMenuList().size()>0) {
//                menuItemsList=MySingelton.getInstance().getMenuList();

//                    productRecyclerAdapter.update(menuItemsList);
            productRecyclerAdapter = new ProductRecyclerAdapter(getActivity(), MySingelton.getInstance().getMenuList(), this);
            rvMenu.setLayoutManager(new LinearLayoutManager(getActivity()));
//              rvMenu.setItemViewCacheSize(20);
//              rvMenu.setDrawingCacheEnabled(true);
            rvMenu.setAdapter(productRecyclerAdapter);
            rlAllIndicator.setVisibility(View.VISIBLE);
            rvCategory.setVisibility(View.VISIBLE);
            rvMenu.setVisibility(View.VISIBLE);
            hideProgress();
            viewMain.setVisibility(View.VISIBLE);




        }
    }
}
