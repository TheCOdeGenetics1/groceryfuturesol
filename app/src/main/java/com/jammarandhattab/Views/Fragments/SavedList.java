package com.jammarandhattab.Views.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jammarandhattab.Models.dbListTitle;
import com.jammarandhattab.Views.Activities.MainActivity;
import com.jammarandhattab.Views.Adapters.SavedListRecyclerAdapter;
import com.jammarandhattab.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SavedList extends Fragment {
    RecyclerView rvSavedList;


    public SavedList() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_saved_list, container, false);
        initComponents(view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        List<dbListTitle> list=new ArrayList<>();
        list= MainActivity.myAppDatabase.dao().getSavedListTitle();

        rvSavedList.setHasFixedSize(true);
        rvSavedList.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false));
        rvSavedList.setAdapter(new SavedListRecyclerAdapter(getActivity(),list));

    }

    private void initComponents(View view) {
        rvSavedList=view.findViewById(R.id.savedlist_rv);
    }

}
