package com.jammarandhattab.Views.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jammarandhattab.Helper.UserSession;
import com.jammarandhattab.Views.Activities.LoginActivity;
import com.jammarandhattab.Views.Activities.Profile;
import com.jammarandhattab.R;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * A simple {@link Fragment} subclass.
 */
public class More extends Fragment {

    TextView tvProfile,tvLogout;


    public More() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_more, container, false);
        initComponents(view);
        clickEvents();
        return view;
    }

    private void clickEvents() {
        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), Profile.class));
            }
        });
        tvLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SweetAlertDialog sweetAlertDialog=   new SweetAlertDialog(getActivity(),SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Logout")
                        .setContentText("Are you sure to logout?")
                        .setConfirmText("Yes")
                        .setCancelText("No")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                UserSession.getInstance(getActivity()).setCustomerID("");
                                UserSession.getInstance(getActivity()).setMobile("");
                                UserSession.getInstance(getActivity()).setEmail("");
                                UserSession.getInstance(getActivity()).setName("");
                                startActivity(new Intent(getActivity(), LoginActivity.class));
                                getActivity().finish();
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        });
                sweetAlertDialog.show();
            }
        });
    }

    private void initComponents(View view) {
       tvProfile=view.findViewById(R.id.more_tvProfile);
       tvLogout=view.findViewById(R.id.more_tvLogout);
    }

}
