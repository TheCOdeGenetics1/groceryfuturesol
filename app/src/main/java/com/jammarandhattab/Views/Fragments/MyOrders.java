package com.jammarandhattab.Views.Fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.jammarandhattab.Helper.Constants;
import com.jammarandhattab.Helper.MySingelton;
import com.jammarandhattab.Helper.UserSession;
import com.jammarandhattab.Models.MyOrders.Menu;
import com.jammarandhattab.Models.MyOrders.MyOrder;
import com.jammarandhattab.Presenters.MyOrderPresenter;
import com.jammarandhattab.R;
import com.jammarandhattab.Utils.RecyclerTouchListener;
import com.jammarandhattab.Views.Activities.OrderDetails;
import com.jammarandhattab.Views.Adapters.MyOrdersRecyler;
import com.jammarandhattab.interfaces.IMyOrders;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyOrders extends Fragment implements IMyOrders.iMain {
    RecyclerView rvMyOrders;
    List<Menu>orderList=new ArrayList<>();
    MyOrderPresenter orderPresenter;
    ProgressBar pb;
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        context = null;
    }

    public MyOrders() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_my_orders, container, false);
        rvMyOrders=view.findViewById(R.id.myorders_rvList);
        orderPresenter=new MyOrderPresenter(this);
        pb=view.findViewById(R.id.pb);

        orderPresenter.getMyOrders(Constants.websiteId, UserSession.getInstance(context).getCustomerID());


        return view;
    }

    @Override
    public void showProgress() {
        pb.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(Throwable t) {
        Log.e(TAG, "showError: ",t.getCause() );
    }

    @Override
    public void setMyOrderResponse(Response<MyOrder> response) {
        if (response.body() != null) {
            orderList = response.body().getMenus();
            rvMyOrders.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            rvMyOrders.setAdapter(new MyOrdersRecyler(context, orderList));
        } else {
            Toast.makeText(context, "No Data Found", Toast.LENGTH_SHORT).show();
        }


        rvMyOrders.addOnItemTouchListener(new RecyclerTouchListener(context, rvMyOrders, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MySingelton.getInstance().setMyOrder(orderList.get(position));
                startActivity(new Intent(context, OrderDetails.class));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    @Override
    public void hideProgress() {
        pb.setVisibility(View.GONE);
    }
}
