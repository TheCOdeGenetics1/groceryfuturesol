package com.jammarandhattab.Views.Fragments;

public class dbAdress {
    private String Title,Adress;

    public dbAdress() {
    }

    public dbAdress(String title, String adress) {
        Title = title;
        Adress = adress;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getAdress() {
        return Adress;
    }

    public void setAdress(String adress) {
        Adress = adress;
    }
}
