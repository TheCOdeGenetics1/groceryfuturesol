package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.LoginResponse;

import retrofit2.Response;

public interface IRegistration {
    interface iMain{
        void showProgress();
        void hideProgress();
        void setLoginResponse(Response<LoginResponse> response);
        void showError(Throwable t);
    }
    interface iPresenter{
        void doRegistration(String name,String mobile,String password);
    }
}
