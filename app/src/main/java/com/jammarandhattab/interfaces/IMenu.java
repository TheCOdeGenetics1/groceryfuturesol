package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.Catgories.Categories;
import com.jammarandhattab.Models.Menu.Menu;

import retrofit2.Response;

public interface IMenu {
    interface iMain{
        void showProgress();
        void showError(Throwable t);
        void setMenu( Response<Menu> response);
        void hideProgress();
    }
    interface iPresenter{
        void getMenu();
        void getMenu(int websiteID,String categorySlug);
        void getMenu(int From,int To);
    }

}
