package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.Menu.Menu;
import com.jammarandhattab.Models.MyOrders.MyOrder;

import retrofit2.Response;

public interface IMyOrders {
    interface iMain{
        void showProgress();
        void showError(Throwable t);
        void setMyOrderResponse( Response<MyOrder> response);
        void hideProgress();
    }
    interface iPresenter{
        void getMyOrders(int websiteID, String customerID);

    }
}
