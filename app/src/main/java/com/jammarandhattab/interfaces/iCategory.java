package com.jammarandhattab.interfaces;


import com.jammarandhattab.Models.Catgories.Categories;

import retrofit2.Response;

public interface iCategory {
    public interface Main{
        void showProgress();
        void showError(Throwable t);
        void setCategories(Response<Categories> response);
        void hideProgress();
    }
    public interface Presenter{
       void getCategories();
    }
}
