package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.photoSlider.PhotoSlider;

import retrofit2.Response;

public interface IPhotoSlider {
    interface iMain{
        void showProgress();
        void hideProgress();
        void setPhotoData(Response<PhotoSlider> response);
        void showError(Throwable t);
    }
    interface iPresenter{
     void getSliderPhotos();
    }
}
