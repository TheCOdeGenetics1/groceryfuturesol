package com.jammarandhattab.interfaces;

public interface IViewClicked {
    void onClick(int position);
}
