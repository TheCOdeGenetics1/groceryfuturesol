package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Models.photoSlider.PhotoSlider;

import retrofit2.Response;

public interface ILogin {

    interface iMain{
        void showProgress();
        void hideProgress();
        void setLoginResponse(Response<LoginResponse> response);
        void showError(Throwable t);
    }
    interface iPresenter{
        void doLogin(String mobile,String password,int websiteid);
        void doRegistration(String mobile,String password,String name,int websiteid);
    }
}
