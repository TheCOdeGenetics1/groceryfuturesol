package com.jammarandhattab.interfaces;

import com.jammarandhattab.Models.AddItemsResponse;
import com.jammarandhattab.Models.CheckoutResponse;
import com.jammarandhattab.Models.Menu.Menu;

import retrofit2.Response;

public interface ICheckOut {
    interface iMain{
        void showProgress();
        void showError(Throwable t);
        void setCheckoutResponse(Response<CheckoutResponse> response);
        void addItemsResponse(Response<AddItemsResponse> response);
        void hideProgress();
    }
    interface iPresenter{
        void checkOut(int websiteID,String  contactID,String coupanCode,String Address,String Landmark,String Fullname,String OrderType,
                      String paymentMethod,String paymentStatus,String totalPrice,String mobile);

        void addItemtoCheckout(int websiteID,int OrderId,String DishId,String unitPrice,String quantity);

    }
}
