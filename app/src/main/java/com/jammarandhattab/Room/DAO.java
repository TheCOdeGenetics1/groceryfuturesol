package com.jammarandhattab.Room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.jammarandhattab.Models.dbListTitle;

import java.util.List;

@Dao
public interface DAO {

    //region CART
    @Insert
    public void addCart(Cart cart);

    @Query("select * from cart")
    public List<Cart> getCart();

    @Query("select * from cart where id=:id")
    public List<Cart>getCartBYId(String id);

    @Query("update cart set Quantity=:Qty where P_Id=:P_Id")
    void updatecart(int Qty,String P_Id);

    @Query("delete from cart where Title=:name")
    void deleteCartByName(String name);

    @Query("delete from cart where P_Id=:P_ID")
    void deleteCartByPid(String P_ID);

    @Query("delete from cart")
    void deleteCart();


    @Query("select * from cart where P_Id=:P_ID")
    public List<Cart> getCartByPid(String P_ID);
    //endregion

    //region SavedList
    @Insert
    public void addintoSavedList(SavedList savedList);

    @Query("select distinct ListTitle,Date from  SavedList")
    public List<dbListTitle> getSavedListTitle();

    @Query("select * from SavedList where ListTitle=:title and Date=:date")
    public List<SavedList> getSavedListByTitleandDate(String title,String date);

    @Query("delete from SavedList where ListTitle=:title and Date=:date")
    public void deleteSavedListbyTitleandDate(String title,String date);
    //endregion
}
