package com.jammarandhattab.Room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
@Database(entities = {Cart.class,SavedList.class},version = 3,exportSchema = false)
public abstract class MyAppDatabase extends RoomDatabase {

    public abstract DAO dao();
}



