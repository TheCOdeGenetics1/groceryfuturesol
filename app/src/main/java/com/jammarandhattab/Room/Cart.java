package com.jammarandhattab.Room;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "Cart")
public class Cart {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "P_Id")
    private String P_Id;
    @ColumnInfo(name = "Title")
    private String Title;
    @ColumnInfo(name = "ShortDescription")
    private int ShortDescription;

    @ColumnInfo(name = "Price")
    private String Price;

    @ColumnInfo(name = "Description")
    private String Description;

    @ColumnInfo(name = "OtherTitle")
    private String OtherTitle;

    @ColumnInfo(name = "Slug")
    private String Slug;

    @ColumnInfo(name = "ImageSource")
    private String ImageSource;

    @ColumnInfo(name = "Thumbnail")
    private String Thumbnail;

    @ColumnInfo(name = "Priority")
    private String Priority;

    @ColumnInfo(name = "MenuCode")
    private String MenuCode;

    @ColumnInfo(name = "OnDiscount")
    private String OnDiscount;

    @ColumnInfo(name = "Discount")
    private String Discount;

    @ColumnInfo(name = "WebsiteId")
    private String WebsiteId;

    @ColumnInfo(name = "CreatedOn")
    private String CreatedOn;

    @ColumnInfo(name = "Category")
    private String Category;

    @ColumnInfo(name = "CategorySlug")
    private String CategorySlug;

    @ColumnInfo(name = "ImageUrl")
    private String ImageUrl;

    @ColumnInfo(name = "Quantity")
    private String Quantity;

    public Cart() {
    }

    public Cart(int id, String p_Id, String title, int shortDescription, String price, String description, String otherTitle, String slug, String imageSource, String thumbnail, String priority, String menuCode, String onDiscount, String discount, String websiteId, String createdOn, String category, String categorySlug, String imageUrl, String quantity) {
        this.id = id;
        P_Id = p_Id;
        Title = title;
        ShortDescription = shortDescription;
        Price = price;
        Description = description;
        OtherTitle = otherTitle;
        Slug = slug;
        ImageSource = imageSource;
        Thumbnail = thumbnail;
        Priority = priority;
        MenuCode = menuCode;
        OnDiscount = onDiscount;
        Discount = discount;
        WebsiteId = websiteId;
        CreatedOn = createdOn;
        Category = category;
        CategorySlug = categorySlug;
        ImageUrl = imageUrl;
        Quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getP_Id() {
        return P_Id;
    }

    public void setP_Id(String p_Id) {
        P_Id = p_Id;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getShortDescription() {
        return ShortDescription;
    }

    public void setShortDescription(int shortDescription) {
        ShortDescription = shortDescription;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getOtherTitle() {
        return OtherTitle;
    }

    public void setOtherTitle(String otherTitle) {
        OtherTitle = otherTitle;
    }

    public String getSlug() {
        return Slug;
    }

    public void setSlug(String slug) {
        Slug = slug;
    }

    public String getImageSource() {
        return ImageSource;
    }

    public void setImageSource(String imageSource) {
        ImageSource = imageSource;
    }

    public String getThumbnail() {
        return Thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        Thumbnail = thumbnail;
    }

    public String getPriority() {
        return Priority;
    }

    public void setPriority(String priority) {
        Priority = priority;
    }

    public String getMenuCode() {
        return MenuCode;
    }

    public void setMenuCode(String menuCode) {
        MenuCode = menuCode;
    }

    public String getOnDiscount() {
        return OnDiscount;
    }

    public void setOnDiscount(String onDiscount) {
        OnDiscount = onDiscount;
    }

    public String getDiscount() {
        return Discount;
    }

    public void setDiscount(String discount) {
        Discount = discount;
    }

    public String getWebsiteId() {
        return WebsiteId;
    }

    public void setWebsiteId(String websiteId) {
        WebsiteId = websiteId;
    }

    public String getCreatedOn() {
        return CreatedOn;
    }

    public void setCreatedOn(String createdOn) {
        CreatedOn = createdOn;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCategorySlug() {
        return CategorySlug;
    }

    public void setCategorySlug(String categorySlug) {
        CategorySlug = categorySlug;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

}
