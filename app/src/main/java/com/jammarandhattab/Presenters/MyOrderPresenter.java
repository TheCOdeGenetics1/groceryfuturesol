package com.jammarandhattab.Presenters;

import com.jammarandhattab.Models.MyOrders.MyOrder;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;
import com.jammarandhattab.interfaces.IMyOrders;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyOrderPresenter implements IMyOrders.iPresenter {
    IMyOrders.iMain iMain;

    public MyOrderPresenter(IMyOrders.iMain iMain) {
        this.iMain = iMain;
    }



    @Override
    public void getMyOrders(int websiteID, String customerID) {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getMyOrder(websiteID,customerID).enqueue(new Callback<MyOrder>() {
            @Override
            public void onResponse(Call<MyOrder> call, Response<MyOrder> response) {
                iMain.hideProgress();
                iMain.setMyOrderResponse(response);

            }

            @Override
            public void onFailure(Call<MyOrder> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });
    }
}
