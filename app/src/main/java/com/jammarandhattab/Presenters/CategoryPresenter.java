package com.jammarandhattab.Presenters;


import com.jammarandhattab.interfaces.iCategory;
import com.jammarandhattab.Models.Catgories.Categories;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryPresenter implements iCategory.Presenter {
    iCategory.Main imain;

    public CategoryPresenter(iCategory.Main imain) {
        this.imain = imain;
    }

    @Override
    public void getCategories() {
        imain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getCategories().enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
//                imain.hideProgress();
                imain.setCategories(response);

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {
                imain.hideProgress();
                imain.showError(t);

            }
        });
    }
}
