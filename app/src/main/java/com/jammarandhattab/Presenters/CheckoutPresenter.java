package com.jammarandhattab.Presenters;

import com.jammarandhattab.Models.AddItemsResponse;
import com.jammarandhattab.Models.CheckoutResponse;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;
import com.jammarandhattab.interfaces.ICheckOut;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckoutPresenter implements ICheckOut.iPresenter {
    ICheckOut.iMain iMain;

    public CheckoutPresenter(ICheckOut.iMain iMain) {
        this.iMain = iMain;
    }

    @Override
    public void checkOut(int websiteID, String contactID, String coupanCode,
                         String Address, String Landmark, String Fullname, String OrderType,
                         String paymentMethod, String paymentStatus, String totalPrice, String mobile) {

        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.doCheckOut(websiteID,contactID,coupanCode,Address,Landmark,Fullname,OrderType,paymentMethod,paymentStatus,
                totalPrice,mobile).enqueue(new Callback<CheckoutResponse>() {
            @Override
            public void onResponse(Call<CheckoutResponse> call, Response<CheckoutResponse> response) {
                iMain.hideProgress();
                iMain.setCheckoutResponse(response);
            }

            @Override
            public void onFailure(Call<CheckoutResponse> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);
            }
        });

    }

    @Override
    public void addItemtoCheckout(int websiteID, int OrderId, String DishId, String unitPrice, String quantity) {
    iMain.showProgress();
    RetrofitInterfaces retrofitInterfaces=RetrofitClient.getClient().create(RetrofitInterfaces.class);
    retrofitInterfaces.addItemsToCheckout(websiteID,OrderId,DishId,unitPrice,quantity).enqueue(new Callback<AddItemsResponse>() {
        @Override
        public void onResponse(Call<AddItemsResponse> call, Response<AddItemsResponse> response) {
            iMain.hideProgress();
            iMain.addItemsResponse(response);
        }

        @Override
        public void onFailure(Call<AddItemsResponse> call, Throwable t) {
            iMain.hideProgress();
            iMain.showError(t);
        }
    });
    }
}
