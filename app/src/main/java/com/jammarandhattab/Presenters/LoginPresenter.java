package com.jammarandhattab.Presenters;

import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;
import com.jammarandhattab.interfaces.ILogin;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements ILogin.iPresenter {
    ILogin.iMain iMain;

    public LoginPresenter(ILogin.iMain iMain) {
        this.iMain = iMain;
    }

    @Override
    public void doLogin(String mobile, String password, int websiteid) {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.doLogin(mobile,password,websiteid).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                iMain.hideProgress();
                iMain.setLoginResponse(response);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });

    }

    @Override
    public void doRegistration(String mobile, String password, String name, int websiteid) {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.doRegistration(mobile,password,name,websiteid).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                iMain.hideProgress();
                iMain.setLoginResponse(response);
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);
            }
        });
    }
}
