package com.jammarandhattab.Presenters;

import com.jammarandhattab.Models.photoSlider.PhotoSlider;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;
import com.jammarandhattab.interfaces.IPhotoSlider;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PhotoSliderPresenter implements IPhotoSlider.iPresenter {
    IPhotoSlider.iMain iMain;

    public PhotoSliderPresenter(IPhotoSlider.iMain iMain) {
        this.iMain = iMain;
    }

    @Override
    public void getSliderPhotos() {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getSLiderphotos().enqueue(new Callback<PhotoSlider>() {
            @Override
            public void onResponse(Call<PhotoSlider> call, Response<PhotoSlider> response) {
                iMain.hideProgress();
                iMain.setPhotoData(response);
            }

            @Override
            public void onFailure(Call<PhotoSlider> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });
    }
}
