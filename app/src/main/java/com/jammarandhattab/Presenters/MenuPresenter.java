package com.jammarandhattab.Presenters;

import com.jammarandhattab.Models.Menu.Menu;
import com.jammarandhattab.Network.RetrofitClient;
import com.jammarandhattab.Network.RetrofitInterfaces;
import com.jammarandhattab.interfaces.IMenu;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuPresenter implements IMenu.iPresenter {
    IMenu.iMain iMain;

    public MenuPresenter(IMenu.iMain iMain) {
        this.iMain = iMain;
    }

    @Override
    public void getMenu() {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getMenu().enqueue(new Callback<Menu>() {
            @Override
            public void onResponse(Call<Menu> call, Response<Menu> response) {
                iMain.hideProgress();
                iMain.setMenu(response);
            }

            @Override
            public void onFailure(Call<Menu> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });
    }

    @Override
    public void getMenu(int websiteID,String categorySlug) {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getMenu(websiteID,categorySlug).enqueue(new Callback<Menu>() {
            @Override
            public void onResponse(Call<Menu> call, Response<Menu> response) {
                iMain.hideProgress();
                iMain.setMenu(response);
            }

            @Override
            public void onFailure(Call<Menu> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });
    }

    @Override
    public void getMenu(int From, int To) {
        iMain.showProgress();
        RetrofitInterfaces retrofitInterfaces= RetrofitClient.getClient().create(RetrofitInterfaces.class);
        retrofitInterfaces.getMenu(From,To).enqueue(new Callback<Menu>() {
            @Override
            public void onResponse(Call<Menu> call, Response<Menu> response) {
                iMain.hideProgress();
                iMain.setMenu(response);
            }

            @Override
            public void onFailure(Call<Menu> call, Throwable t) {
                iMain.hideProgress();
                iMain.showError(t);

            }
        });
    }
}
