package com.jammarandhattab.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static  final String BASE_URL="http://api.jwh.ae/api/";
    private static Retrofit retrofit;
    public static Retrofit getClient()
    {
        if (retrofit==null) {
            Retrofit.Builder builder = new Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
            retrofit = builder.build();
            return retrofit;
        }
        else {
            return retrofit;
        }
    }
}
