package com.jammarandhattab.Network;


import com.jammarandhattab.Models.AddItemsResponse;
import com.jammarandhattab.Models.Catgories.Categories;
import com.jammarandhattab.Models.CheckoutResponse;
import com.jammarandhattab.Models.LoginResponse;
import com.jammarandhattab.Models.Menu.Menu;
import com.jammarandhattab.Models.MyOrders.MyOrder;
import com.jammarandhattab.Models.photoSlider.PhotoSlider;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitInterfaces {

    @GET("RestaurantApi/GetMenuCategories?WebsiteId=1")
    Call<Categories>getCategories();


    @GET("RestaurantApi/GetAllMenus?WebsiteId=1")
    Call<Menu>getMenu();

    @GET("RestaurantApi/GetMenus")
    Call<Menu>getMenu(@Query("WebsiteId")int websiteId, @Query("CategorySlug") String categorySlug);

    @GET("RestaurantApi/GetAllMenuData?WebsiteId=1")
    Call<Menu>getMenu(@Query("From")int from,@Query("To")int to);

    @GET("ThemeApi/GetSlider?WebsiteId=1&Slug=mobile-app-slider")
    Call<PhotoSlider>getSLiderphotos();

    @GET("RegistrationApi/LoginByMobile")
    Call<LoginResponse>doLogin(
            @Query("Mobile") String mobile,
            @Query("Password")String password,
            @Query("WebsiteId") int websiteID);

    @GET("RegistrationApi/RegisterByMobile")
    Call<LoginResponse>doRegistration(
            @Query("Mobile") String mobile,
            @Query("Password")String password,
            @Query("name")String name,
            @Query("WebsiteId") int websiteID);

    @GET("RestaurantAPI/CreateOrder")
    Call<CheckoutResponse> doCheckOut(
            @Query("WebsiteId") int websiteID,
            @Query("ContactId") String contactID,
            @Query("CounponCode")String coupanCode,
            @Query("Address")String Address,
            @Query("Landmark")String Landmark,
            @Query("FullName")String FullName,
            @Query("OrderType")String OrderType,
            @Query("PaymentMethod")String PaymentMethod,
            @Query("PaymentStatus")String PaymentStatus,
            @Query("TotalPrice")String TotalPrice,
            @Query("mobile")String mobile);

    @GET("RestaurantAPI/AddOrderItem")
    Call<AddItemsResponse> addItemsToCheckout(
            @Query("WebsiteId") int websiteID,
            @Query("OrderId") int orderID,
            @Query("DishId") String p_id,
            @Query("UnitPrice") String unitPrice,
            @Query("Qty")String quantity
    );

    @GET("RestaurantAPI/GetCustomerOrder")
    Call<MyOrder>getMyOrder(
            @Query("WebsiteId" )int websiteID,
            @Query("CustomerId") String customerID
    );
}
