package com.jammarandhattab.Models.MyOrders;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MyOrder {

    @SerializedName("Menus")
    @Expose
    private List<Menu> menus = null;

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }

}


