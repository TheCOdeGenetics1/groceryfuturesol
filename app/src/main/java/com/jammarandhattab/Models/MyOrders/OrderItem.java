package com.jammarandhattab.Models.MyOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderItem {

    @SerializedName("OrderItemId")
    @Expose
    private Integer orderItemId;
    @SerializedName("Qty")
    @Expose
    private Integer qty;
    @SerializedName("NetTotal")
    @Expose
    private Integer netTotal;
    @SerializedName("NetWeight")
    @Expose
    private Integer netWeight;
    @SerializedName("Unitprice")
    @Expose
    private Integer unitprice;
    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("ProductId")
    @Expose
    private Integer productId;

    public Integer getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(Integer orderItemId) {
        this.orderItemId = orderItemId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Integer getNetTotal() {
        return netTotal;
    }

    public void setNetTotal(Integer netTotal) {
        this.netTotal = netTotal;
    }

    public Integer getNetWeight() {
        return netWeight;
    }

    public void setNetWeight(Integer netWeight) {
        this.netWeight = netWeight;
    }

    public Integer getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(Integer unitprice) {
        this.unitprice = unitprice;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

}
