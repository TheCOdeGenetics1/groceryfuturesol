package com.jammarandhattab.Models.MyOrders;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Menu {

    @SerializedName("OrderId")
    @Expose
    private Integer orderId;
    @SerializedName("OrderType")
    @Expose
    private String orderType;
    @SerializedName("DishId")
    @Expose
    private Integer dishId;
    @SerializedName("PaymentMethod")
    @Expose
    private String paymentMethod;
    @SerializedName("OrderCode")
    @Expose
    private String orderCode;
    @SerializedName("CouponCode")
    @Expose
    private String couponCode;
    @SerializedName("ContactId")
    @Expose
    private Integer contactId;
    @SerializedName("OrderStatus")
    @Expose
    private String orderStatus;
    @SerializedName("TotalPrice")
    @Expose
    private Integer totalPrice;
    @SerializedName("OrderForFullName")
    @Expose
    private String orderForFullName;
    @SerializedName("OrderForAddressLine1")
    @Expose
    private String orderForAddressLine1;
    @SerializedName("OrderForAddressLine2")
    @Expose
    private String orderForAddressLine2;
    @SerializedName("DateCreated")
    @Expose
    private String dateCreated;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("OrderItems")
    @Expose
    private List<OrderItem> orderItems = null;
    @SerializedName("WebsiteId")
    @Expose
    private Integer websiteId;

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public Integer getDishId() {
        return dishId;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getContactId() {
        return contactId;
    }

    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getOrderForFullName() {
        return orderForFullName;
    }

    public void setOrderForFullName(String orderForFullName) {
        this.orderForFullName = orderForFullName;
    }

    public String getOrderForAddressLine1() {
        return orderForAddressLine1;
    }

    public void setOrderForAddressLine1(String orderForAddressLine1) {
        this.orderForAddressLine1 = orderForAddressLine1;
    }

    public String getOrderForAddressLine2() {
        return orderForAddressLine2;
    }

    public void setOrderForAddressLine2(String orderForAddressLine2) {
        this.orderForAddressLine2 = orderForAddressLine2;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public Integer getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Integer websiteId) {
        this.websiteId = websiteId;
    }

}
