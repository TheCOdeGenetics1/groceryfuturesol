package com.jammarandhattab.Models;

public class dbproduct {
    private  String category,title,titleU,price,image,Qty;

    public dbproduct() {
    }

    public dbproduct(String category, String title, String titleU, String price, String image, String qty) {
        this.category = category;
        this.title = title;
        this.titleU = titleU;
        this.price = price;
        this.image = image;
        Qty = qty;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleU() {
        return titleU;
    }

    public void setTitleU(String titleU) {
        this.titleU = titleU;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }
}
