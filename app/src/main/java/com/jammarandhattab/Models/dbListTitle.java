package com.jammarandhattab.Models;

public class dbListTitle {
    String ListTitle, Date;

    public dbListTitle() {
    }

    public dbListTitle(String listTitle, String date) {
        ListTitle = listTitle;
        Date = date;
    }

    public String getListTitle() {
        return ListTitle;
    }

    public void setListTitle(String listTitle) {
        ListTitle = listTitle;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
