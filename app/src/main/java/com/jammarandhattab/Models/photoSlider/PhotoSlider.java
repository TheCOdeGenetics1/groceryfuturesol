package com.jammarandhattab.Models.photoSlider;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PhotoSlider {

    @SerializedName("Slider")
    @Expose
    private Slider slider;

    public Slider getSlider() {
        return slider;
    }

    public void setSlider(Slider slider) {
        this.slider = slider;
    }

}


