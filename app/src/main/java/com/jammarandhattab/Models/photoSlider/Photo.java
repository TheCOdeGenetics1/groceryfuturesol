package com.jammarandhattab.Models.photoSlider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Photo {

    @SerializedName("PhotoId")
    @Expose
    private Integer photoId;
    @SerializedName("PhotoSource")
    @Expose
    private String photoSource;
    @SerializedName("PhotoTitle")
    @Expose
    private String photoTitle;
    @SerializedName("PhotoUrl")
    @Expose
    private String photoUrl;
    @SerializedName("PhotoContent")
    @Expose
    private Object photoContent;
    @SerializedName("UploadedOn")
    @Expose
    private String uploadedOn;
    @SerializedName("PhotoType")
    @Expose
    private Object photoType;

    public Integer getPhotoId() {
        return photoId;
    }

    public void setPhotoId(Integer photoId) {
        this.photoId = photoId;
    }

    public String getPhotoSource() {
        return photoSource;
    }

    public void setPhotoSource(String photoSource) {
        this.photoSource = photoSource;
    }

    public String getPhotoTitle() {
        return photoTitle;
    }

    public void setPhotoTitle(String photoTitle) {
        this.photoTitle = photoTitle;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Object getPhotoContent() {
        return photoContent;
    }

    public void setPhotoContent(Object photoContent) {
        this.photoContent = photoContent;
    }

    public String getUploadedOn() {
        return uploadedOn;
    }

    public void setUploadedOn(String uploadedOn) {
        this.uploadedOn = uploadedOn;
    }

    public Object getPhotoType() {
        return photoType;
    }

    public void setPhotoType(Object photoType) {
        this.photoType = photoType;
    }

}
