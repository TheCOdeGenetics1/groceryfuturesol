package com.jammarandhattab.Models.photoSlider;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Slider {

    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Priority")
    @Expose
    private Object priority;
    @SerializedName("OtherTitle")
    @Expose
    private Object otherTitle;
    @SerializedName("ImageSource")
    @Expose
    private Object imageSource;
    @SerializedName("ShortDescription")
    @Expose
    private Object shortDescription;
    @SerializedName("WebsiteId")
    @Expose
    private Integer websiteId;
    @SerializedName("Category")
    @Expose
    private Object category;
    @SerializedName("CategorySlug")
    @Expose
    private Object categorySlug;
    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Slug")
    @Expose
    private String slug;
    @SerializedName("Thumbnail")
    @Expose
    private Object thumbnail;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("Photos")
    @Expose
    private List<Photo> photos = null;
    @SerializedName("ThumbnailUrl")
    @Expose
    private Object thumbnailUrl;

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getPriority() {
        return priority;
    }

    public void setPriority(Object priority) {
        this.priority = priority;
    }

    public Object getOtherTitle() {
        return otherTitle;
    }

    public void setOtherTitle(Object otherTitle) {
        this.otherTitle = otherTitle;
    }

    public Object getImageSource() {
        return imageSource;
    }

    public void setImageSource(Object imageSource) {
        this.imageSource = imageSource;
    }

    public Object getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(Object shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Integer getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Integer websiteId) {
        this.websiteId = websiteId;
    }

    public Object getCategory() {
        return category;
    }

    public void setCategory(Object category) {
        this.category = category;
    }

    public Object getCategorySlug() {
        return categorySlug;
    }

    public void setCategorySlug(Object categorySlug) {
        this.categorySlug = categorySlug;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Object getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Object thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public Object getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(Object thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

}
