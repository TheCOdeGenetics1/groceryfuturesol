package com.jammarandhattab.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddItemsResponse {
    @SerializedName("OrderID")
    @Expose
    private Integer orderID;
    @SerializedName("MenuID")
    @Expose
    private Integer menuID;

    public Integer getOrderID() {
        return orderID;
    }

    public void setOrderID(Integer orderID) {
        this.orderID = orderID;
    }

    public Integer getMenuID() {
        return menuID;
    }

    public void setMenuID(Integer menuID) {
        this.menuID = menuID;
    }
}
