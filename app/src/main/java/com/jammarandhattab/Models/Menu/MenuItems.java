package com.jammarandhattab.Models.Menu;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuItems {

    @SerializedName("Id")
    @Expose
    private Integer id;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ShortDescription")
    @Expose
    private Object shortDescription;
    @SerializedName("Price")
    @Expose
    private Integer price;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("OtherTitle")
    @Expose
    private String otherTitle;
    @SerializedName("Slug")
    @Expose
    private String slug;
    @SerializedName("ImageSource")
    @Expose
    private String imageSource;
    @SerializedName("Thumbnail")
    @Expose
    private String thumbnail;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("MenuCode")
    @Expose
    private Object menuCode;
    @SerializedName("OnDiscount")
    @Expose
    private Boolean onDiscount;
    @SerializedName("Discount")
    @Expose
    private Integer discount;
    @SerializedName("WebsiteId")
    @Expose
    private Integer websiteId;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("CategorySlug")
    @Expose
    private String categorySlug;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;
    @SerializedName("ThumbnailUrl")
    @Expose
    private String thumbnailUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(Object shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public String getOtherTitle() {
        return otherTitle;
    }

    public void setOtherTitle(String otherTitle) {
        this.otherTitle = otherTitle;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Object getMenuCode() {
        return menuCode;
    }

    public void setMenuCode(Object menuCode) {
        this.menuCode = menuCode;
    }

    public Boolean getOnDiscount() {
        return onDiscount;
    }

    public void setOnDiscount(Boolean onDiscount) {
        this.onDiscount = onDiscount;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Integer websiteId) {
        this.websiteId = websiteId;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategorySlug() {
        return categorySlug;
    }

    public void setCategorySlug(String categorySlug) {
        this.categorySlug = categorySlug;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }
}
