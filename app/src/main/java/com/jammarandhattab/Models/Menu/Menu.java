package com.jammarandhattab.Models.Menu;



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Menu {

    @SerializedName("Menus")
    @Expose
    private List<MenuItems> menus = null;

    public List<MenuItems> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuItems> menus) {
        this.menus = menus;
    }

}


