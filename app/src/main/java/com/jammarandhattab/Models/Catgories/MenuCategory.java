package com.jammarandhattab.Models.Catgories;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MenuCategory {

    @SerializedName("CategoryId")
    @Expose
    private Integer categoryId;
    @SerializedName("CategoryTitle")
    @Expose
    private String categoryTitle;
    @SerializedName("OtherTitle")
    @Expose
    private String otherTitle;
    @SerializedName("CategorySlug")
    @Expose
    private String categorySlug;
    @SerializedName("ImageSource")
    @Expose
    private Object imageSource;
    @SerializedName("ShortDescription")
    @Expose
    private Object shortDescription;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("WebsiteId")
    @Expose
    private Integer websiteId;
    @SerializedName("Priority")
    @Expose
    private Integer priority;
    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;
    @SerializedName("ImageUrl")
    @Expose
    private String imageUrl;

    private boolean isSelected = false;

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getOtherTitle() {
        return otherTitle;
    }

    public void setOtherTitle(String otherTitle) {
        this.otherTitle = otherTitle;
    }

    public String getCategorySlug() {
        return categorySlug;
    }

    public void setCategorySlug(String categorySlug) {
        this.categorySlug = categorySlug;
    }

    public Object getImageSource() {
        return imageSource;
    }

    public void setImageSource(Object imageSource) {
        this.imageSource = imageSource;
    }

    public Object getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(Object shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getWebsiteId() {
        return websiteId;
    }

    public void setWebsiteId(Integer websiteId) {
        this.websiteId = websiteId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
