package com.jammarandhattab.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MCategory {
    @SerializedName("CategoryTitle")
    @Expose
    private String categoryTitle;
    @SerializedName("CategorySlug")
    @Expose
    private String categorySlug;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("ShortDescription")
    @Expose
    private Object shortDescription;
    @SerializedName("Photo")
    @Expose
    private String photo;
    @SerializedName("Priority")
    @Expose
    private Integer priority;

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getCategorySlug() {
        return categorySlug;
    }

    public void setCategorySlug(String categorySlug) {
        this.categorySlug = categorySlug;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Object getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(Object shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
