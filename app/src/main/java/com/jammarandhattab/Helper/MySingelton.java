package com.jammarandhattab.Helper;

import com.jammarandhattab.Models.Catgories.MenuCategory;
import com.jammarandhattab.Models.Menu.MenuItems;
import com.jammarandhattab.Models.MyOrders.Menu;
import com.jammarandhattab.Models.MyOrders.MyOrder;
import com.jammarandhattab.Models.photoSlider.Photo;

import java.util.ArrayList;
import java.util.List;

public class MySingelton {

    public static MySingelton instance=new MySingelton();

    private List<MenuItems>menuList=new ArrayList<>();
    private List<MenuCategory>categoryList=new ArrayList<>();
    private List<Photo>sliderPhotos=new ArrayList<>();
    private com.jammarandhattab.Models.MyOrders.Menu myOrder=new Menu();

    public static MySingelton getInstance() {
        return instance;
    }

    public static void setInstance(MySingelton instance) {
        MySingelton.instance = instance;
    }

    public List<MenuItems> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuItems> menuList) {
        this.menuList = menuList;
    }

    public List<MenuCategory> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<MenuCategory> categoryList) {
        this.categoryList = categoryList;
    }

    public List<Photo> getSliderPhotos() {
        return sliderPhotos;
    }

    public void setSliderPhotos(List<Photo> sliderPhotos) {
        this.sliderPhotos = sliderPhotos;
    }

    public Menu getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(Menu myOrder) {
        this.myOrder = myOrder;
    }
}
