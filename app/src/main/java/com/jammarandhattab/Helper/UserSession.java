package com.jammarandhattab.Helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class UserSession {

    private SharedPreferences sp;
    private SharedPreferences.Editor spEditor;
    private Context mContext;



    public UserSession(Context context) {
        if (context != null) {
            sp = PreferenceManager.getDefaultSharedPreferences(context);
            this.mContext = context;
        }
    }


    private static UserSession instance;

    public static UserSession getInstance(Context context) {

        if (instance == null) {
            instance = new UserSession(context);
        }
        return instance;
    }

    public String getMobile() {
        return sp.getString("Mobile", "");
    }


    public boolean setMobile(String mobile) {
        spEditor = sp.edit();
        spEditor.putString("Mobile", mobile);
        spEditor.apply();
        return true;
    }

    public String getName() {
        return sp.getString("Name", "");
    }


    public boolean setName(String name) {
        spEditor = sp.edit();
        spEditor.putString("Name", name);
        spEditor.apply();
        return true;
    }

    public String getEmail() {
        return sp.getString("Email", "");
    }


    public boolean setEmail(String email) {
        spEditor = sp.edit();
        spEditor.putString("Email", email);
        spEditor.apply();
        return true;
    }

    public String getCustomerID() {
        return sp.getString("ID", "");
    }


    public boolean setCustomerID(String customerID) {
        spEditor = sp.edit();
        spEditor.putString("ID", customerID);
        spEditor.apply();
        return true;
    }

}
